<!DOCTYPE html>
<style>
.pull-left{
	width:10%;
div {
  width: 200px;
}
 
h2 {
  font: 400 40px/1.5 Helvetica, Verdana, sans-serif;
  margin: 0;
  padding: 0;
}
 
ul {
  list-style-type: none;
  margin: 0;
  padding: 0;
}
 
li {
  font: 200 20px/1.5 Helvetica, Verdana, sans-serif;
  border-bottom: 1px solid #ccc;
}
 
li:last-child {
  border: none;
}
 
li a {
  text-decoration: none;
  color: #000;
  display: block;
  width: 200px;
 
  -webkit-transition: font-size 0.3s ease, background-color 0.3s ease;
  -moz-transition: font-size 0.3s ease, background-color 0.3s ease;
  -o-transition: font-size 0.3s ease, background-color 0.3s ease;
  -ms-transition: font-size 0.3s ease, background-color 0.3s ease;
  transition: font-size 0.3s ease, background-color 0.3s ease;
}
 
li a:hover {
  font-size: 30px;
  background: #f6f6f6;
}
	
	
}



</style>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  
<link rel="stylesheet" href="css/font-awesome.min.css">
</head>
<body>


<legend align="center"> <h2 align="center"><i class="fa fa-spinner fa-spin" style="font-size:24px"></i>&nbsp;Our Happy Clients&nbsp;<i class="fa fa-spinner fa-spin" style="font-size:24px"></i></h2></legend>
<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
  <!-- Wrapper for slides -->
  <div class="carousel-inner">
  
    <div class="item active container">

         <a href="shoppage.php"><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/chat.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/102.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/103.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/104.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/105.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/101.jpg" alt="..."></a>

    </div>
    
    <div class="item container">
          <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/101.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/102.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/103.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/104.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/105.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/101.jpg" alt="..."></a>
      </div>
    
    <div class="item container">
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/101.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/102.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/103.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/104.jpg" alt="..."></a>
         <a href="#" ><img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/105.jpg" alt="..."></a>
        <a href="#" > <img class="col-md-2 col-sm-2 col-xs-2 logo img-responsive" src="images/101.jpg" alt="..."></a>
     </div>
    
  </div>

    <div align="center" style="margin-top:20px;">
    <!-- Controls -->
          <a class="left" href="#carousel-example-generic" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left"></span>
          </a>
          <a class="right" href="#carousel-example-generic" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right"></span>
          </a>
    </div>
  
</div>
<div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-content">
            <div class="close-modal" data-dismiss="modal">
                <div class="lr">
                    <div class="rl">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2">
                        <div class="modal-body">
                            <h2>Fruit Shop</h2>
                            <hr class="star-primary">
                            <img src="images/food.jpg"  class="img-responsive img-centered" alt="">
                           <strong><a href="#"><h3>Fresh Food Shop</h3></a>
                            <ul class="list-inline item-details">
                                <li>
                                    <strong><a href="#"><h3>Address</h3></a>
                                    </strong>
                                
                            </ul>
                            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</body>
</html>
