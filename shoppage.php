<?php 
	session_start();
	include('config.php');
	$_SESSION['shopid']=$_GET['id'];
?>
<!DOCTYPE html> 

<html>
<head>

	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="assets/ico/rsz_final.gif">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="bootstrap/css/header.css">
  <link rel="stylesheet" href="assets/css/shoppage.css">
  
  <!-- for google map-->
  <script src="http://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7/html5shiv.js"></script>
  <script src="http://cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.js"></script>
  
        <script type="text/javascript" src="jquery/jquery.js"></script>
        
		
		<script src="bootstrap/js/bootstrap.min.js"></script> 
  
 
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
  <script src="http://maps.google.com/maps/api/js?sensor=true"></script>

  <script>
	function calculateRoute(form,to)
	{
		var myOptions={
			zoom:10,
			center: new google.maps.LatLng(40.84,14.25),
			mapTypeId: google.maps.MapTypeId.ROADMAP
		};
		var mapObject=new google.maps.Map(document.getElementById("map"),myOptions);
		
		var directionsService=new google.maps.DirectionsService();
		var directionsRequest={
			origin:from,
			destination:to,
			travelMode:google.maps.DirectionsTravelMode.DRIVING,
			unitSystem:google.maps.UnitSystem.METRIC
		};
		directionsService.route(
			directionsRequest,
			function(response,status)
			{
				if(status==google.maps.DirectionsStatus.OK)
				{
					new google.maps.DirectionRenderer({
						map:mapObject,
						directions:response
					});
				}
				else
				{
					$("#error").append("Unable to retrive your route");
				}
			}
			);
	}
	$(document).ready(function()
	{
		if(typeof navigator.geolocation=="undefined")
		{
			$("#error").text('your browser doesnt support google API');
			return;
		}
		
		$("#from-link,#to-link").click(function(event)
		{
			evenet.preventDefault();
			var addressId=this.id.substring(0,this.id.indexOf("-"));
			
			navigator.geolocation.getCurrentPosition(function(position)
			{
				var geocoder=new google.maps.Geocoder();
				geocoder.geocode({
					"location":new google.maps.LatLng(position.croods.latitude,position.croods.longitude)
				},
				function(results,status)
				{
					if(status==google.maps.GeocoderStatus.OK)
					{
						$("#"+addressId).val(results[0].formatted_address);
					}
					else
					{
						$("#error").append("Unable to retrive your address<br />");
					}
				});
			},
			function(postionError){
				$("#error").append("Error: " +positionError.message + "<br />");
			},
			{
				enableHighAccuracy:true,
				timeout: 10 * 1000
			});
		});
		
		$("#calculate-route").submit(function(event){
			event.preventDefault();
			calculateRoute($("#from").val(),$("#to").val());
		});
	});
  </script>
  <style>
	#map
	{
		width:500px;
		height:400px;
	}
  </style>
</head>
<body>
        <header>
				
	    </header>
		
	

	    
			<div class="shop-container">
				
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
							<!-- Indicators -->
								<ol class="carousel-indicators">
									<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
									<li data-target="#myCarousel" data-slide-to="1"></li>
									<li data-target="#myCarousel" data-slide-to="2"></li>
      
								</ol>

											<!-- Wrapper for slides -->
										<div class="carousel-inner" role="listbox">
											<?php 
												$str="SELECT * FROM img WHERE shop_id=".$_SESSION['shopid'];
												$res=mysqli_query($conn,$str);
												$cnt=0;
												while($row=mysqli_fetch_array($res))
												{
													
													if($cnt==0)
													{
											?>
													<div class="item active">
															<img src="admin/images/<?php echo $row['img'];?>" alt="MY SHOP" style="width:960px;height:645px;" >
															<div class="carousel-caption">
																
															</div>
													</div>
												<?php 
													}
													else
													{
												?>
												
													<div class="item">
															<img src="admin/images/<?php echo $row['img'];?>" alt="MY SHOP" style="width:960px;height:645px;">
															<div class="carousel-caption">
																
															</div>
													</div>
												<?php
													}
													$cnt++;
													}
												?>
													
										</div>

											<!-- Left and right controls -->
								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
									<span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
										<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
								</a>
						</div>
			</div>
			
		
    <?php
	 //echo $_GET['id'];
	 $str1="SELECT * FROM shop_detail WHERE id=".$_SESSION['shopid'];
	 $res1=mysqli_query($conn,$str1);
	 $row1=mysqli_fetch_array($res1);
	?>        
	<div class="container">

        <!-- shop icon section -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
                    <?php echo $row1['name'];?>
                </h1>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-check"></i> Contact</h4>
                    </div>
                    <div class="panel-body">
						
							<i class="glyphicon glyphicon-phone" style="font-size:20px;">:</i><span style="font-size:20px;"><?php echo $row1['contact'];?></span><br><br>
							<br><br><br>
                        
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-gift"></i> Services</h4>
                    </div>
                    <div class="panel-body">
                             <ul style="font-size:18px";>
                <?php
                                $srv=explode(",",$row1['services']);
                                foreach($srv as $value)     
                                {
                                    ?>
                                    <li> <?php echo "$value";?></li>        
                            <?php
								}
                            ?>
				</ul>
                        <!--span class="glyphicon glyphicon-apple" style="font-size:20px";><?php //echo $row1['services'];?></span-->
						<!--ul>
							<li>pani puri,dahipuri</li>
							<li>shev puri,chat</li>
							<li>ragda patis,bhel</li>
											   
						</ul--><br>
                    </div>
                </div>
            </div>
			<?php
				$str2="SELECT cat_id FROM subcat WHERE id=".$row1['subcat_id'];
				$res2=mysqli_query($conn,$str2);
				$row2=mysqli_fetch_array($res2);
				
				$str3="SELECT name FROM category WHERE id=".$row2['cat_id'];
				$res3=mysqli_query($conn,$str3);
				$row3=mysqli_fetch_array($res3);
				
			?>
            <div class="col-md-4">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4><i class="fa fa-fw fa-compass"></i> Catagory</h4>
                    </div>
                    <div class="panel-body">
                        <span class="glyphicon glyphicon-asterisk" style="font-size:20px";><?php echo $row3['name'];?></span>
						<br><br><br><br><br>
                    </div>
                </div>
            </div>
        </div>
		<!--address and map-->
		<div class="row">
            <div class="col-lg-12">
                <h4 class="page-header"><b>Address:</b></h4>
            </div>
            <div class="col-md-6">
			<ul style="font-size:18px;">
                <?php
                                $adr=explode(",",$row1['addr']);
                                foreach($adr as $value)     
                                {
                                    ?>
                                    <li> <?php echo "$value";?></li>        
                            <?php
								}
                            ?>
			</ul>				
                <!--ul style="font-size:18px"; >
                    <li>near ganesh furniture,</li>
                    <li>maharaja complex paud road,</li>
                    <li>Kothrud,Pune 38</li>
                                       
                </ul-->
                <p></p>
            </div>
			<form class="claculate-route" name="calculate-route" >
				<label for="from">From:</label>
				<input type="text" id="from" required="required" placeholder="An Address" size="30">
				<a href="#" id="from-link">Get my position</a>
				<br />
				<label for="to">To:</label>
				<input type="text" id="to" name="to" size="30" required="required" value="<?php echo $row1['addr']?>" disabled> 
				<br />
				
				<input type="submit" />
				<input type="reset" />
			</form>
            <div id="map-container" class="col-md-6" style="display:block;">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15133.878059304416!2d73.800734!3d18.5076732!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x4cd1d525e7c6797d!2sGanesh+Furniture!5e0!3m2!1sen!2sin!4v1474306738186"></iframe>
					</div>
			
			</div>
			  
        </div>
		
		
	</div>
	    <footer> 
 
					<div class="row">
							<div class="col-lg-12">
								<p>Copyright &copy; hives online india pvt ltd.</p>
							</div>
					</div>
			

		</footer>

	</section> 


</body>

</html>