<!DOCTYPE html>
<html>
	
<!-- Mirrored from ashobiz.asia/boot-extended15/ui/ui-93.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Jan 2016 09:50:03 GMT -->
<head>
		<meta charset="utf-8">
		<title>UI Design</title>
		<!-- Description, Keywords and Author -->
		<meta name="description" content="Your description">
		<meta name="keywords" content="Your,Keywords">
		<meta name="author" content="ResponsiveWebInc">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<!-- Styles -->
		<!-- Bootstrap CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- Font awesome CSS -->
		<link href="css/font-awesome.min.css" rel="stylesheet">
		
		
		<!-- Main CSS -->
		<link href="css/style-93.css" rel="stylesheet">
				
		<!-- Favicon -->
		<link rel="shortcut icon" href="#">
	</head>
<style>
	
.nav_custom{

    height: 400px;
    width:250px;
	font-size:12px;
	padding-left:0.5px;
	padding-right:0.5px;
	text-align:left;

    margin-bottom: 1px;

    -webkit-column-count: 2;
       -moz-column-count: 2;
            column-count: 2;
         
     
}

	img {
    display: block;
    margin: 0 auto;
     } 
h1{
	color:white;
	font-size:50px;	
}

	
		.jumbotron {
    background-color:#916dd1 !important; 
      text-align:center;
}
  

input-group stylish-input-group{
	   text-align:center;
}
h6{
	color:black;
}


li:hover
{
	font-size:12.5px;
	background-color:#dda0dd;;
}
background-color:#e0e0e0; 
}


  body{
	background: !important;   
  }
b{
	color:#4285F4;
	}	 

}





</style>
	
	<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="background-color:#673ab7">
        <div class="container purple topBotomBordersIn">
            <image class="responsive-image"src="images/final.gif" height="60" width="60" style="margin-left:-80px;margin-top:8px;">
            <!-- Collect the nav links, forms, and other content for toggling -->
           
				
                <ul class="nav navbar-nav" style="margin-top:-50px;">
                    <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                    
                    <li>
                        <a  href="index1.php" style="color:white">HOME</a>
                    </li>
                    <li>
                        <a  href="#about" style="color:white">ABOUT</a>
                    </li>
                    <li>
                        <a  href="#contact" style="color:white">CONTACT</a>
                    </li>
                </ul>
      
  </div>
</nav>

    


    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide">
      <div class="carousel-inner">
        <div class="item active">
          <img src="images/1.jpg">
          <div class="container">
            <div class="carousel-caption">
              <strong><h1>You Know Us.People Will Know You.</h1></strong>
              <p class="lead">Welcome to eshopmarket. Which will connect everything.</p>
              <a class="btn btn-large btn-primary" href="signup.php">Sign up today</a>
              
     <div class="container">
	<div class="row">
        <div class="col-sm-8">
            <div id="imaginary_container"> 
                <div class="input-group stylish-input-group">
                    <input type="text" class="form-control"  placeholder="Search" >
                    <span class="input-group-addon">
                        <button type="submit">
                            <span class="glyphicon glyphicon-search"></span>
                        </button>  
                    </span>
                </div>
            </div>
        </div>
	</div>
</div>
            </div>
          </div>
        </div>
        
        <div class="item">
          <img src="images/2.jpg" alt="">
          <div class="container">
            <div class="carousel-caption">
              <strong><h1>You Know Us.People Will Know You.</h1></strong>
              <p class="lead">Welcome to eshopmarket. Which will connect everything.</p>
              <a class="btn btn-large btn-primary" href="#">Learn more</a>
                <div class="container">
					<div class="row">
						<div class="col-sm-8">
							<div id="imaginary_container"> 
								<div class="input-group stylish-input-group">
									<input type="text" class="form-control"  placeholder="Search" >
										<span class="input-group-addon">
											<button type="submit" >
											<span class="glyphicon glyphicon-search"></span>
											</button>  
									</span>
								</div>
							</div>
					</div>
				</div>
			</div>
           </div>
         </div>
     </div>
     
        
   </div>
      <a class="left carousel-control" href="#myCarousel" data-slide="prev">�</a>
      <a class="right carousel-control" href="#myCarousel" data-slide="next">�</a>
    </div><!-- /.carousel -->
		<!-- UI - X Starts -->
	
		
		<div class="ui-93">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>HEALTH</b>
								<!-- Paragraph -->
								<img src="images/doctor.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/doctor.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-user-md" style="font-size:20px;color:black"></i>&nbsp;Doctor</a></li>
								<li><a href="#"><i class="fa fa-hospital-o" style="font-size:20px;color:black">&nbsp;</i>Hospital</a></li>
								<li><a href="#"><i class="fa fa-medkit" style="font-size:20px;color:black">&nbsp;</i>Medical</a></li>
								<li><a href="#"><i class="fa fa-arrows-alt" style="font-size:20px;color:black">&nbsp;</i>X-Ray Center</a></li>
								<li><a href="#"><i class="fa fa-hospital-o" style="font-size:20px;color:black"></i>&nbsp;Pathology Lab</a></li>
								<li><a href="#"><center><i class="fa fa-hospital-o" style="font-size:20px;color:black"></i>&nbsp;Gents Gym Center</center></a></li>
								<li><a href="#"><center><i class="material-icons" style="font-size:20px;color:black">group</i>&nbsp;Unisex Gym Center</center></a></li>
								<li><a href="#"><center><i class="material-icons" style="font-size:20px;color:black">accessibility</i>Yoga Center</center></a></li>
						

								</ul>
							</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Food</b>
								<!-- Paragraph -->
								<img src="images/food1.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/food1.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-building" style="font-size:20px;color:black"></i>&nbsp;Hotel</a></li>
								<li><a href="#"><i class="fa fa-building" style="font-size:20px;color:black"></i>&nbsp;Snacks Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">location_city</i>&nbsp;Sweet Home	</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:brown">cake</i>Bakery Shop</a></li>
								<li><a href="areapage.php"><i class="material-icons" style="font-size:20px;color:black">home</i>Chat Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">home</i>Ice-Cream </a></li>
								<li><a href="#"><i class="fa fa-leaf" style="font-size:20px;color:green"></i>&nbsp;Pan Shop</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">kitchen</i>&nbsp;Mess</a></li>
								<li><a href="#"><span class="glyphicon glyphicon-grain "></i>Grainshop</a></li>
								</ul>
							</span>
						</div>
					</div>
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Vehicle</b>
								<!-- Paragraph -->
								<img src="images/vehicle.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/vehicle.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-car" style="font-size:20px;color:black"></i>&nbsp;Car Showroom</a></li>
								<li><a href="#"><i class="fa fa-motorcycle" style="font-size:20px;color:black"></i>&nbsp;Bike Showroom</a></li>
								<li><a href="#"><i class="fa fa-wrench" style="font-size:20px;color:black"></i>&nbsp;Car Service Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:blue">local_car_wash</i>&nbsp;Washing Center</a></li>
								<li><a href="#"><i class="fa fa-wrench" style="font-size:20px;color:black"></i>&nbsp;Garage</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">fitness_center</i>
									Accessories</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">add_circle</i>Automobile</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">fitness_center</i>
								Tempo Service</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">add_circle</i>Car-SUV-Minibus-Taxi </a></li>

								</ul>
							</span>
						</div>
					</div>
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Cloths</b>
								<!-- Paragraph -->
								<img src="images/cloth1.jpeg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/cloth1.jpeg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">group</i>&nbsp;Family</a></li>
								<li><a href="#"><i class="fa fa-male" style="font-size:20px;color:black"></i>&nbsp;Gents Wear</a></li>
								<li><a href="#"><i class="fa fa-female" style="font-size:20px;color:green"></i>&nbsp;Ladies Wear</a></li>
								<li><a href="#"><i class="fa fa-group" style="font-size:20px;color:black"></i>&nbsp;Kids Wear</a></li>
								<li><a href="#"><i class="fa fa-street-view" style="font-size:20px;color:blue"></i>&nbsp;Seasonal Wear</a></li>
								<li><a href="#"><i class="fa fa-male" style="font-size:20px;color:black"></i>&nbsp;Gents Tailor</a></li>
								<li><a href="#"><i class="fa fa-female" style="font-size:20px;color:black"></i>&nbsp;Ladies Tailor</a></li>
						

								</ul>
							</span>
						</div>
					</div>
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Footwear	</b>
								<!-- Paragraph -->
								<img src="images/foot.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/foot.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>Family Store</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>Cobblers</a></li>

								</ul>
							</span>
						</div>
					</div>
				<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Home and Kitchen</b>
								<!-- Paragraph -->
								<img src="images/home1.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/home1.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>&nbsp;Furniture</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>&nbsp;Home Furnishing</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>&nbsp;Kitchen Furnishing</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>&nbsp;Carpenter</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>&nbsp;Electronics Store</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>Electronics Service Center</a></li>
								<li><a href="#"><i class="fa fa-angle-double-right" style="font-size:20px;color:black"></i>Gas and Care</a></li>
					
								</ul>
							</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Education</b>
								<!-- Paragraph -->
								<img src="images/education.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/education.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">toys</i>Play School</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">toys</i>Nursery</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>School</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>College</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Tution</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Personal Tutor</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Project Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Technical Institude</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Consultancy Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">laptop</i>Cyber Cafe</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">local_library</i>Libraries</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:">local_library</i>Book Stall</a></li>
							    <li><a href="#"><i class="material-icons" style="font-size:20px;color:black">school</i>Fashion College</a></li>

								</ul>
							</span>
						</div>
					</div>
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Shop/Stationary</b>
								<!-- Paragraph -->
								<img src="images/stationary.png" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/stationary.png" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">book</i>Dairy Shop</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">home</i>Stationary</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">home</i>Super Market</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">home</i>Jewelry Shop</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">camera_alt</i>Photo Studio</a></li>
								<li><a href="#">   <span class="glyphicon glyphicon-sunglasses" style="font-size:20px;color:black"></span>Optical Store</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">watch</i>Watch Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">power</i>Electrical Shop</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">print</i>Printing press</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">print</i>Xerox Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">print</i>Hardware Store</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">print</i>Radium Work</a></li>
						
						

								</ul>
							</span>
						</div>
					</div>
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Body Care</b>
								<!-- Paragraph -->
								<img src="images/body.jpg" height="200" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/body.jpg" height="90" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-male" style="font-size:20px;color:black"></i>&nbsp;Gents Salon</a></li>
								<li><a href="#"><i class="fa fa-female" style="font-size:20px;color:black"></i>&nbsp;Ladies Beauty Parlor</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">spa</i>
								Spa Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">airline_seat_flat</i>Body Massage Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">accessibility</i>
								Body Care Center</a></li>
						

								</ul>
							</span>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Mobile/Computer</b>
								<!-- Paragraph -->
								<img src="images/computer.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/computer.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">phonelink_ring</i>
									Mobile Retail Shop</a></li>
								<li><a href="#"><li><a href="#"><i class="material-icons" style="font-size:20px;color:black">phone_android</i>Recharge Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">computer</i>
								Computer and Laptop Center</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">laptop</i>
								Service Center</a></li>
							
						

								</ul>
							</span>
						</div>
					</div>
					
						<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Company</b>
								<!-- Paragraph -->
								<img src="images/company.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/company.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-building" style="font-size:17px;color:black"></i>Software Company</a></li>
								<li><a href="#"><i class="fa fa-building" style="font-size:17px;color:black"></i>Hardware Product Base Company</a></li>
								<li><a href="#"><i class="fa fa-building" style="font-size:17px;color:black"></i>Advertising Company</a></li>
				

								</ul>
							</span>
						</div>
					</div>
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Entertainment</b>
								<!-- Paragraph -->
								<img src="images/Entertainment.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/Entertainment.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-video-camera" style="font-size:20px;color:black"></i>&nbsp;Theater</a></li>
								<li><a href="#"><i class="fa fa-group" style="font-size:20px;color:black"></i>&nbsp;Party Event</a></li>
								<li><a href="#"><i class="fa fa-headphones" style="font-size:20px;color:black"></i>&nbsp;Pub & Disco</a></li>
								<li><a href="#"><i class="fa fa-building-o" style="font-size:16px;color:black"></i>&nbsp;Functional Halls</a></li>
								<li><a href="#"><i class="fa fa-building-o" style="font-size:16px;color:black"></i>Functional Lawns</a></li>
								<li><a href="#"><i class="material-icons" style="font-size:20px;color:black">content_cut</i>Costume Designer</a></li>
								<li><a href="#"><i class="fa fa-male" style="font-size:20px;color:black"></i>&nbsp;Event Manager</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-music" style="color:black"></i>DJ</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-music" style="color:black"></i>Music Institude</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-music" style="color:black"></i>Music Teacher</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-music" style="color:black"></i>Music Institude</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-music" style="color:black"></i>Singers</a></li>
								<li><a href="#"><i class="glyphicon glyphicon-camera" style="color:black"></i>Photographer</a></li>
								</ul>
							</span>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Helpers</b>
								<!-- Paragraph -->
								<img src="images/helper.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/helper.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Real Estate Agents</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;RTO Documentation</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Gas Connection</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Water Supplier</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Ceremony Decoration</a></li>
	
								</ul>
							</span>
						</div>
					</div>
					
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Legal</b>
								<!-- Paragraph -->
								<img src="images/law.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/law.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><i class="fa fa-user" style="font-size:20px;color:black"></i><a href="#">&nbsp;Lawyer</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Notary</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Charted Accountant</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Tax Consultant</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Company Secreatory</a></li>
	
								</ul>
							</span>
						</div>
					</div>
					
								<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Home Care</b>
								<!-- Paragraph -->
								<img src="images/plumber.jpg" height="130" width="250"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/plumber.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
							 <ul class="nav_custom">
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Dry Cleaning Center</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Iron Center</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Pest Control</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Paintor</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Raddiwala</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;Plumber</a></li>
								<li><a href="#"><i class="fa fa-user" style="font-size:20px;color:black"></i>&nbsp;News Paper</a></li>
								</ul>
							</span>
						</div>
					</div
					<div class="col-md-3 col-sm-3 col-xs-6 col-bor col-mob">
						<!-- Item -->
						<div class="ui-item ui-hbg-white">
							<!-- Hover One -->
							<span class="ui-hover-one">
								<!-- Heading -->
								<b>Travel</b>
								<!-- Paragraph -->
								<img src="images/travel.jpg" height="150" width="200"  align="center">
							</span>
							<!-- Hover Two -->
							<span class="ui-hover-two">
								<!-- Heading -->
							<img src="images/travel.jpg" height="60" width="90"  align="center">
								<!-- Paragraph -->
				
							</span>
						</div>
					</div
					
					
					
			</div>	
		</div>
		<!-- UI - X Ends -->
		
<?php
  include("shopslide.php");
		?>
		
		
    <!-- About Section -->
    <section class="success" id="about">
		<div class="jumbotron">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>About Us</h2>
                    <hr class="star-light">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
					<h3>Looking out for nearby local shops for services?</h3>
                    <p><small>Welcome to the e-shopMarket ,an online directory of all the local shops and services. Its the one and only place you will everneed to look out for local shops andservices information which includes details like contact number,website,owner info,working hour and a lot more.</small></p>
                    <hr class="star-light">
                </div>
                
                <div class="col-lg-12">
					<h3>Our Mission</h3>
                    <p><small>To connect every local shop and service to the needful customer.</small></p><hr class="star-light">
                </div>
                <div class="col-lg-12">
					<h3><i style="font-size:24px" class="fa">&#xf062;</i> Bottom to top approach</h3>
                    <p><small>We are deeply committed to connect every shop from grassroot level to the top level domain with the customer at the right time for the right need..</small></p><hr class="star-light">
                </div>
                 <div class="col-lg-12">
					<h3><i style="font-size:24px" class="fa">&#xf156;</i>&nbsp;Free Of Cost Service for customers</h3>
                    <p><small><i style="font-size:24px" class="fa">&#xf0a4;</i>&nbsp;With almost thousands of shops and services listed ,search for whatever you need at absolutely no cost,yes we are offering it all for free.</small></p>
					<p><small><i style="font-size:24px" class="fa">&#xf0a4;</i>&nbsp;For Shop owners we charge very less amount of registration fees which is negligible as compared to putting yourshops on the internet.</small></p>
					<p><small><i style="font-size:24px" class="fa">&#xf0a4;</i>&nbsp; So next time whenever you want information about any we really mean any local shop or service ,just visit us and you are done..!</small></p>
                </div>
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <a href="#" class="btn btn-lg btn-outline">
              
                    </a>
                </div>
            </div>
        </div>
        </div>
    </section>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2>Contact Us</h2>
                    <hr class="star-primary">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <!-- To configure the contact form email address, go to mail/contact_me.php and update the email address in the PHP file on line 19. -->
                    <!-- The form should work on most web servers, but if the form is not working you may need to configure your web server differently. -->
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Name</label>
                                <input type="text" class="form-control" placeholder="Name" id="name" required data-validation-required-message="Please enter your name.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Email Address</label>
                                <input type="email" class="form-control" placeholder="Email Address" id="email" required data-validation-required-message="Please enter your email address.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Phone Number</label>
                                <input type="tel" class="form-control" placeholder="Phone Number" id="phone" required data-validation-required-message="Please enter your phone number.">
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <div class="row control-group">
                            <div class="form-group col-xs-12 floating-label-form-group controls">
                                <label>Message</label>
                                <textarea rows="5" class="form-control" placeholder="Message" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                <p class="help-block text-danger"></p>
                            </div>
                        </div>
                        <br>
                        <div id="success"></div>
                        <div class="row">
                            <div class="form-group col-xs-12">
                                <button type="submit" class="btn btn-primary btn-lg">Send</button><hr class="star-light">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
		
		<!-- Javascript files -->
		<!-- jQuery -->
		<script src="js/jquery.js"></script>
		<!-- Bootstrap JS -->
		<script src="js/bootstrap.min.js"></script>
		<!-- Placeholder JS -->
		<script src="js/placeholder.js"></script>
		<!-- Respond JS for IE8 -->
		<script src="js/respond.min.js"></script>
		<!-- HTML5 Support for IE -->
		<script src="js/html5shiv.js"></script>
	</body>	

<!-- Mirrored from ashobiz.asia/boot-extended15/ui/ui-93.html by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 06 Jan 2016 09:50:14 GMT -->
  <!-- Footer -->
    <footer class="text-center">
        <div class="footer-above">
            <div class="container">
                <div class="row">
                    <div class="footer-col col-md-4">
                        <h3>Pune</h3>
                        <p>Balewadi
                            <br></p>
                    </div>
                    <div class="footer-col col-md-4">
                        <h3>Around the Web</h3>
                        <ul class="list-inline">
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-google-plus"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-linkedin"></i></a>
                            </li>
                            <li>
                                <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-dribbble"></i></a>
                            </li>
                        </ul>
                    </div>
                  
                </div>
            </div>
        </div>
        <div class="footer-below">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; Your Website 2016
                    </div>
                </div>
            </div>
        </div>
    </footer>

</html>
