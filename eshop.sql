-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2016 at 05:20 AM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.5.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminlog`
--

CREATE TABLE `adminlog` (
  `id` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminlog`
--

INSERT INTO `adminlog` (`id`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `area`
--

CREATE TABLE `area` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `area`
--

INSERT INTO `area` (`id`, `name`, `img`, `city_id`) VALUES
(1, 'Baner', 'work1.jpg', 1),
(2, 'Sangavi', 'work2.jpg', 1),
(3, 'Balewadi', 'work3.jpg', 1),
(4, 'Wakad', 'work4.jpg', 1),
(5, 'Pimple Gurav', 'work1.jpg', 1),
(6, 'Pimpri', 'work2.jpg', 1),
(7, 'Chinchwad', 'work3.jpg', 1),
(8, 'Akurdi', 'work4.jpg', 1),
(9, 'Kotharud', 'work1.jpg', 1),
(10, 'Karvenagar', 'work2.jpg', 1),
(11, 'Varje', 'work3.jpg', 1),
(12, 'Pashan', 'work4.jpg', 1),
(13, 'Hadapsar', 'had.jpg', 1),
(14, 'aaa', '', 2);

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `name`, `img`) VALUES
(1, 'HEALTH', 'doctor.jpg'),
(2, 'FOOD', 'food1.jpg'),
(3, 'VEHICLE', 'vehicle.jpg'),
(4, 'CLOTHS', 'cloth1.jpeg'),
(5, 'FOOTWARE', 'foot.jpg'),
(6, 'HOME AND KITCHEN', 'home1.jpg'),
(7, 'EDUCATION', 'education.jpg'),
(8, 'SHOP/STATIONARY', 'stationary.png'),
(9, 'BODY CARE', 'body.jpg'),
(10, 'MOBILE/COMPUTER', 'computer.jpg'),
(11, 'COMPANY', 'company.jpg'),
(12, 'ENTERTAINMENT', 'Entertainment.jpg'),
(13, 'HELPERS', 'helper.jpg'),
(14, 'LEGAL', 'law.jpg'),
(15, 'HOME CARE', 'plumber.jpg'),
(16, 'TRAVEL', 'travel.jpg'),
(17, 'aaa', '');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`) VALUES
(1, 'Pune'),
(2, 'Mumbai'),
(3, 'aaa');

-- --------------------------------------------------------

--
-- Table structure for table `dummy_img`
--

CREATE TABLE `dummy_img` (
  `id` int(11) NOT NULL,
  `img` varchar(500) NOT NULL,
  `shop_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dummy_img`
--

INSERT INTO `dummy_img` (`id`, `img`, `shop_id`, `flag`) VALUES
(1, 'hospital.jpg', 2, 1),
(2, 'hos.jpg', 2, 1),
(3, 'doc2.jpg', 2, 0),
(4, 'rai3.jpg', 4, 0),
(5, 'rai2.jpg', 4, 0),
(6, 'rai1.jpg', 4, 1),
(7, 'm1.jpg', 6, 0),
(8, 'm2.jpg', 6, 0),
(9, 'm3.jpg', 6, 0),
(10, 'm1.jpg', 7, 0),
(11, 'm2.jpg', 7, 0),
(12, 'm3.jpg', 7, 0),
(13, 'm1.jpg', 8, 0),
(14, 'm2.jpg', 8, 0),
(15, 'm3.jpg', 8, 0),
(16, 'm1.jpg', 9, 0),
(17, 'm2.jpg', 9, 0),
(18, 'm3.jpg', 9, 0),
(19, 'doctor11.jpg', 15, 0),
(20, 'doctor12.jpg', 15, 0),
(21, 'doctor13.jpg', 15, 0),
(22, 'sonu.jpg', 12, 0),
(23, 'sonu1.jpg', 12, 0),
(24, 'sonu2.jpg', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `dummy_shop_detail`
--

CREATE TABLE `dummy_shop_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `services` varchar(500) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL,
  `img_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dummy_shop_detail`
--

INSERT INTO `dummy_shop_detail` (`id`, `name`, `addr`, `img`, `contact`, `services`, `subcat_id`, `area_id`, `owner_id`, `flag`, `img_flag`) VALUES
(2, 'satish', 'sanghavi', 'dhokla.jpg', 9665315635, 'doc', 1, 9, 12, 1, 1),
(3, 'Dr.Priyanshe', 'Kothrud', 'work6.jpg', 9881243093, 'doc', 1, 9, 16, 0, 0),
(4, 'Rai Tours and Travels', 'Krishna Complex,Near Sangram Press,Kothrud,Pune.', 'rai.jpg', 25421701, 'Tours and Tours', 114, 9, 31, 0, 0),
(6, 'Krushna Furnishing', 'Dhanlakshmi Park,Bhusari Colony,Poud Road,Kothrud,Pune', 'kru.jpg', 25282999, 'Bed Material', 36, 9, 14, 0, 0),
(7, 'Sonskruti Furniture Household Accessories', 'Lunawat Reality,Opp. Vanaz Factory,Near Hotel Senator,Paud Road,Pune', 'san.jpg', 25385951, 'Furniture', 36, 9, 32, 0, 0),
(8, 'Disha Furniture', 'Kothrud,Pune', 'maha.jpg', 1234567890, 'Home and Kitchen', 36, 9, 33, 0, 0),
(9, 'Devi Furniture', 'Kothrud,Pune', 'maha.jpg', 123567890, 'Home and Kitchen', 36, 9, 34, 0, 0),
(10, 'gfchg', 'fghgj', '2005-bmw-k12s-d-07-1[1].jpg', 12345678, 'chg,dfgh,dhf', 2, 1, 35, 0, 0),
(11, 'gfchg', 'fghgj', 'Hydrangeas.jpg 	', 12345678, 'chg', 1, 1, 36, 0, 0),
(12, 'Sonu Chat Center', 'akurdi', 'work8.jpg', 1234567890, 'panipuri,shevpuri,dahishevpuri,ragada', 13, 8, 37, 0, 0),
(13, 'abc', 'fghgj', '3aa.jpg', 1234567890, 'chg', 2, 2, 9, 0, 0),
(14, 'radhe', 'aundh', '001-gsx1300r-rbFS.jpg', 1236547890, 'medicine,24hours availibility,etc', 1, 1, 10, 0, 0),
(15, 'kalpesh', 'Baner Road', '1 (47).JPG', 1234567890, 'kjhckj,dfg,drh,', 2, 2, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `img`
--

CREATE TABLE `img` (
  `id` int(11) NOT NULL,
  `img` varchar(500) NOT NULL,
  `shop_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `img`
--

INSERT INTO `img` (`id`, `img`, `shop_id`) VALUES
(1, 'doc2.jpg', 2),
(2, 'doc1.jpg', 2),
(3, 'hospital.jpg', 2),
(4, 'rai1.jpg', 4),
(5, 'rai2.jpg', 4),
(6, 'rai3.jpg', 4),
(7, 'm1.jpg', 6),
(8, 'm2.jpg', 6),
(9, 'm3.jpg', 6),
(10, 'm1.jpg', 7),
(11, 'm2.jpg', 7),
(12, 'm3.jpg', 7),
(13, 'm1.jpg', 8),
(14, 'm2.jpg', 8),
(15, 'm3.jpg', 8),
(16, 'm1.jpg', 9),
(17, 'm2.jpg', 9),
(18, 'm3.jpg', 9),
(19, 'tailer.jpg', 14),
(20, 'tailer2.jpg', 14),
(21, 'tailer3.jpg', 14),
(22, 'doctor11.jpg', 15),
(23, 'doctor12.jpg', 15),
(24, 'doctor13.jpg', 15),
(25, 'sonu.jpg', 12),
(26, 'sonu1.jpg', 12),
(27, 'sonu2.jpg', 12);

-- --------------------------------------------------------

--
-- Table structure for table `moderator`
--

CREATE TABLE `moderator` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `contact` bigint(10) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `img` varchar(200) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moderator`
--

INSERT INTO `moderator` (`id`, `name`, `addr`, `contact`, `email`, `password`, `img`, `flag`) VALUES
(1, 'abc', 'fghgj', 12345678, 'abc@xyz.com', 'abc', '; (36).jpg', 1),
(2, 'kalpesh', 'sanghavi', 1234567890, 'kalpesh@hiveslab.com', 'kalpesh', 'modprofile.jpg', 1),
(4, 'Rakesh', 'Sanghvi', 1234567890, 'rakeshhiveslab@gmail.com', 'rakabhai', 'modprofile.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modlog`
--

CREATE TABLE `modlog` (
  `id` varchar(500) NOT NULL,
  `pass` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modlog`
--

INSERT INTO `modlog` (`id`, `pass`) VALUES
('mod', 'mod');

-- --------------------------------------------------------

--
-- Table structure for table `shop_detail`
--

CREATE TABLE `shop_detail` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `addr` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `services` varchar(500) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_detail`
--

INSERT INTO `shop_detail` (`id`, `name`, `addr`, `img`, `contact`, `services`, `subcat_id`, `area_id`, `owner_id`) VALUES
(2, 'satish', 'sanghavi', 'dhokla.jpg', 9665315635, 'doc', 1, 9, 12),
(3, 'Dr.Priyanshe', 'Kothrud', 'work6.jpg', 9881243093, 'doc', 1, 9, 16),
(4, 'Rai Tours and Travels', 'Krishna Complex,Near Sangram Press,Kothrud,Pune.', 'rai.jpg', 25421701, 'Tours and Tours', 114, 9, 31),
(6, 'Krushna Furnishing', 'Dhanlakshmi Park,Bhusari Colony,Poud Road,Kothrud,Pune', 'kru.jpg', 25282999, 'Bed Material', 36, 9, 14),
(7, 'Sonskruti Furniture Household Accessories', 'Lunawat Reality,Opp. Vanaz Factory,Near Hotel Senator,Paud Road,Pune', 'san.jpg', 25385951, 'Furniture', 36, 9, 32),
(8, 'Disha Furniture', 'Kothrud,Pune', 'maha.jpg', 1234567890, 'Home and Kitchen', 36, 9, 33),
(9, 'Devi Furniture', 'Kothrud,Pune', 'maha.jpg', 123567890, 'Home and Kitchen', 36, 9, 34),
(10, 'gfchg', 'fghgj', '2005-bmw-k12s-d-07-1[1].jpg', 12345678, 'chg,dfgh,dhf', 2, 1, 35),
(11, 'gfchg', 'fghgj', 'Hydrangeas.jpg', 12345678, 'chg', 1, 1, 36),
(12, 'Sonu Chat Center', 'akurdi', 'work8.jpg', 1234567890, 'panipuri,shevpuri,dahishevpuri,ragada', 13, 8, 37),
(13, 'abc', 'fghgj', '3aa.jpg', 1234567890, 'chg', 2, 2, 9),
(14, 'Kachins', 'Akurdi', 'tailer1.jpg', 1234567890, 'chg,dfgh,dhf', 32, 8, 0),
(15, 'radhe', 'aundh', '001-gsx1300r-rbFS.jpg', 1236547890, 'medicine,24hours availibility,etc', 1, 1, 10),
(16, 'kalpesh', 'Baner Road', '1 (47).JPG', 1234567890, 'kjhckj,dfg,drh,', 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shop_details`
--

CREATE TABLE `shop_details` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `email` varchar(500) NOT NULL,
  `services` varchar(500) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `mod_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_details`
--

INSERT INTO `shop_details` (`id`, `name`, `addr`, `img`, `contact`, `email`, `services`, `subcat_id`, `area_id`, `mod_id`, `owner_id`) VALUES
(32, 'fsdlkgm', 'ytfdtufj', 'Penguins.jpg', 123456, 'abc@xyz.com', 'chg', 1, 1, 2, 5),
(33, 'Arun', 'balewadi', '00Fsupra.jpg', 9874563210, 'abc@gmail.com', 'medicine,24hours availibility,etc', 1, 1, 2, 6),
(34, 'Sadanad', 'Baner Road', '001.jpg', 9874563210, 'qwerty@gmail.com', 'food service,home delievery,fast food,continental', 9, 9, 2, 7),
(35, 'jbikjb', 'hb', '001-gsx1300r-rbFS.jpg', 12345678, 'abc@xyz.com', 'chg,dfgh,dhf', 1, 1, 2, 8),
(39, 'star hospital', 'krishna chowk', '00Fsupra.jpg', 1234567890, 'abc@gmail.com', 'medicine,24hours availibility,etc', 2, 2, 1, 11),
(40, 'RM Mobiles', 'Sanghavi', 'mobile.jpg', 1234567890, 'rakesh@hiveslab.com', 'Mobile Recharge,TV Recharge', 74, 74, 2, 29),
(41, 'RM Mobiles', 'Akurdi', 'mobile.jpg', 1234567890, 'rakesh@hiveslab.com', 'Mobile Recharge,TV Recharge', 74, 74, 2, 30);

-- --------------------------------------------------------

--
-- Table structure for table `shop_img`
--

CREATE TABLE `shop_img` (
  `id` int(11) NOT NULL,
  `img` varchar(500) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_img`
--

INSERT INTO `shop_img` (`id`, `img`, `owner_id`) VALUES
(7, 'moscow_russia_4k_5k-1920x1080.jpg', 4),
(8, 'birla1.jpg', 4),
(9, 'birla2.jpg', 4),
(10, 'image11.jpg', 15),
(11, 'image22.jpg', 15),
(12, 'image44.jpg', 15),
(16, 'lunch.jpg', 17),
(17, '9.jpg', 17),
(18, 'r8.jpg', 17),
(19, 'mobile1.jpg', 29),
(20, 'mobile2.jpg', 29),
(21, 'mobile3.jpg', 29),
(22, 'mobile3.jpg', 30),
(23, 'mobile2.jpg', 30),
(24, 'mobile1.jpg', 30);

-- --------------------------------------------------------

--
-- Table structure for table `shop_owner`
--

CREATE TABLE `shop_owner` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `contact` varchar(500) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `email` varchar(500) NOT NULL,
  `pass` varchar(500) NOT NULL,
  `status` varchar(500) NOT NULL,
  `flag` int(11) NOT NULL,
  `img_flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_owner`
--

INSERT INTO `shop_owner` (`id`, `name`, `contact`, `addr`, `email`, `pass`, `status`, `flag`, `img_flag`) VALUES
(1, 'abc', '1234567890', 'akurdi', 'NA', '123', 'approve', 1, 1),
(2, 'rahul', '1234567890', 'Baner Road', 'NA', '123', 'pending', 1, 0),
(3, 'ramesh', '97845612301', 'baner', 'abc@gmail.com', '9874563210', 'pending', 1, 0),
(4, 'birla hospital', '1234567890', 'Baner Road', 'birlahospital@gmail.com', 'birla123', 'pending', 1, 1),
(5, 'abc1', '1234567890', 'pune', 'abc@xxz.com', '12345', 'pending', 1, 0),
(6, 'abc2', '1234567890', 'pune', 'xyz@abc.com', '12345', 'pending', 1, 0),
(7, 'abc3', '1234567890', 'punw', 'xxyy@x.com', '12345', 'pending', 1, 0),
(8, 'abc4', '1234567890', 'pune', 'aa@bb.com', '12345', 'pending', 1, 0),
(9, 'abc4', '1234567890', 'pune', 'xx@yy.com', '12345', 'approve', 1, 0),
(10, 'abc5', '1234567890', 'pune', 'aa@bb.com', '12345', 'approve', 1, 1),
(11, 'abc6', '1234567890', 'pune', 'bb@aa.com', '12345', 'pending', 1, 0),
(12, 'abc7', '1234567890', 'pune', 'bb@aa.com', '12345', 'approve', 1, 1),
(13, 'abc8', '1234567890', 'pune', 'bb@aa.com', '12345', 'approve', 1, 1),
(14, 'abc9', '1234567890', 'pune', 'bb@aa.com', '12345', 'approve', 1, 1),
(15, 'sonu', '1234567890', 'akurdi', 'NA', '12345', 'pending', 1, 1),
(16, 'rajendra', '9078563412', 'akurdi', 'raj@gmail.com', '12345', 'approve', 1, 0),
(17, 'Sangram', '1234567890', 'Akurdi', 'sangram@gmail.com', '123456', 'pending', 1, 1),
(18, 'Kachins', '1234567890', 'Akurdi', 'Kachins@gmail.com', '12345', 'pending', 1, 1),
(29, 'Rakesh', '1234567890', 'Sanghavi', 'rakesh@hiveslab.com', '12345', 'pending', 1, 1),
(30, 'Rakesh', '1234567890', 'Akurdi', 'rakesh@hiveslab.com', '12345', 'pending', 1, 1),
(31, 'Mugdha', '1234567890', 'Kothrud', 'mugdhajoshi1111@gmail.com', '12345', 'approve', 1, 1),
(32, 'Jaydeep', '1234567890', 'Dehu', 'NA', '12345', 'approve', 1, 1),
(33, 'Vishal More', '1234567890', 'Akurdi', 'more.vishal.45@gmail.com', '12345', 'approve', 1, 1),
(34, 'Suraj', '1234567890', 'Kalewadi', 'surajkharote.sk@gmail.com', '12345', 'approve', 1, 1),
(35, 'Pankaj', '7276112301', 'nigdi', 'pankaj.bhokse@gmail.com', '12345', 'approve', 1, 0),
(36, 'Yogesh Giri', '1234567890', 'Nigdi', 'yogesh.giri@gmail.com', '12345', 'approve', 1, 0),
(37, 'Tejas', '9503444394', 'Nigdi', 'tejas.thorve@gmail.com', '12345', 'approve', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `shop_owner_details`
--

CREATE TABLE `shop_owner_details` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `addr` varchar(500) NOT NULL,
  `img` varchar(500) NOT NULL,
  `contact` bigint(20) NOT NULL,
  `email` varchar(500) NOT NULL,
  `services` varchar(500) NOT NULL,
  `subcat_id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `flag` int(11) NOT NULL,
  `dis` int(11) NOT NULL,
  `mod_id` int(11) NOT NULL,
  `owner_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shop_owner_details`
--

INSERT INTO `shop_owner_details` (`id`, `name`, `addr`, `img`, `contact`, `email`, `services`, `subcat_id`, `area_id`, `flag`, `dis`, `mod_id`, `owner_id`) VALUES
(2, 'abcd', 'Baner Road', '001-gsx1300r-rbFS.jpg', 12345678, 'abc@xyz.com', 'chg,dfgh,dhf', 19, 1, 0, 1, 2, 2),
(3, 'ramesh', 'baner', '001.jpg', 411045, 'abc@gmail.com', 'breakfast,lunch,dinner,party orders', 9, 1, 1, 0, 2, 3),
(4, 'Birla Hospital', 'Chinchwad', '001.jpg', 1234567890, 'birlahospital@gmail.com', 'medicine,24hours availibility,etc', 2, 7, 0, 0, 2, 4),
(5, 'Geeta Pavbhaji', 'Akurdi', 'r5.jpg', 1234567890, 'sangram@gmail.com', 'Pavbhaji,Snaks', 9, 8, 0, 0, 0, 17);

-- --------------------------------------------------------

--
-- Table structure for table `subcat`
--

CREATE TABLE `subcat` (
  `id` int(11) NOT NULL,
  `name` varchar(500) NOT NULL,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subcat`
--

INSERT INTO `subcat` (`id`, `name`, `cat_id`) VALUES
(1, 'Doctor', 1),
(2, 'Hospital', 1),
(3, 'Medical', 1),
(4, 'X-Ray Center', 1),
(5, 'Pathology Lab', 1),
(6, 'Gents Gym Center', 1),
(7, 'Unisex Gym Center', 1),
(8, 'Yoga Center', 1),
(9, 'Hotel', 2),
(10, 'Snacks Center', 2),
(11, 'Sweet Home', 2),
(12, 'Bakery Shop', 2),
(13, 'Chat Center', 2),
(14, 'Ice-Cream', 2),
(15, 'Pan Shop', 2),
(16, 'Mess', 2),
(17, 'Grainshop', 2),
(18, 'Car Showroom', 3),
(19, 'Bike Showroom', 3),
(20, 'Car Service Center', 3),
(21, 'Washing Center', 3),
(22, 'Garage', 3),
(23, 'Accessories', 3),
(24, 'Automobile', 3),
(25, 'Tempo Service', 3),
(26, 'Car-SUV-Minibus-Taxi', 3),
(27, 'Family', 4),
(28, 'Gents Wear', 4),
(29, 'Ladies Wear', 4),
(30, 'Kids Wear', 4),
(31, 'Seasonal Wear', 4),
(32, 'Gents Tailor', 4),
(33, 'Ladies Tailor', 4),
(34, 'Family Store', 5),
(35, 'Cobblers', 5),
(36, 'Furniture', 6),
(37, 'Home Furnishing', 6),
(38, 'Kitchen Furnishing', 6),
(39, 'Carpenter', 6),
(40, 'Electronics Store', 6),
(41, 'Electronics Service Center', 6),
(42, 'Gas and Care', 6),
(43, 'Play School', 7),
(44, 'Nursery', 7),
(45, 'School', 7),
(46, 'College', 7),
(47, 'Tution', 7),
(48, 'Personal Tutor', 7),
(49, 'Project Center', 7),
(50, 'Technical Institude', 7),
(51, 'Consultancy Center', 7),
(52, 'Cyber Cafe', 7),
(53, 'Libraries', 7),
(54, 'Book Stall', 7),
(55, 'Fashion College', 7),
(56, 'Dairy Shop', 8),
(57, 'Stationary', 8),
(58, 'Super Market', 8),
(59, 'Jewelry Shop', 8),
(60, 'Photo Studio', 8),
(61, 'Optical Store', 8),
(62, 'Watch Center', 8),
(63, 'Electrical Shop', 8),
(64, 'Printing press', 8),
(65, 'Xerox Center', 8),
(66, 'Hardware Store', 8),
(67, 'Radium Work', 8),
(68, 'Gents Salon', 9),
(69, 'Ladies Beauty Parlor', 9),
(70, 'Spa Center', 9),
(71, 'Body Massage Center', 9),
(72, 'Body Care Center', 9),
(73, 'Mobile Retail Shop', 10),
(74, 'Recharge Center', 10),
(75, 'Computer and Laptop Center', 10),
(76, 'Service Center', 10),
(77, 'Software Company', 11),
(78, 'Hardware Product Base Company', 11),
(79, 'Advertising Company', 11),
(80, 'Theater', 12),
(81, 'Party Event', 12),
(82, 'Pub & Disco', 12),
(83, 'Functional Halls', 12),
(84, 'Functional Lawns', 12),
(85, 'Costume Designer', 12),
(86, 'Event Manager', 12),
(87, 'DJ', 12),
(88, 'Music Institude', 12),
(89, 'Music Teacher', 12),
(90, 'Singers', 12),
(91, 'Photographer', 12),
(92, 'Real Estate Agents', 13),
(93, 'RTO Documentation', 13),
(94, 'Gas Connection', 13),
(95, 'Water Supplier', 13),
(96, 'Ceremony Decoration', 13),
(97, 'Lawyer', 14),
(98, 'Notary', 14),
(99, 'Charted Accountant', 14),
(100, 'Tax Consultant', 14),
(101, 'Company Secreatory', 14),
(102, 'Dry Cleaning Center', 15),
(103, 'Iron Center', 15),
(104, 'Pest Control', 15),
(105, 'Paintor', 15),
(106, 'Raddiwala', 15),
(107, 'Plumber', 15),
(108, 'News Paper', 15),
(109, 'Chapati Center', 2),
(110, 'Penting', 13),
(111, 'Beauty Parlor', 13),
(112, 'Windows', 13),
(113, 'Microwave', 13),
(114, 'Tours And Travel', 16),
(115, 'abc', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `area`
--
ALTER TABLE `area`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dummy_img`
--
ALTER TABLE `dummy_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dummy_shop_detail`
--
ALTER TABLE `dummy_shop_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `img`
--
ALTER TABLE `img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `moderator`
--
ALTER TABLE `moderator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_detail`
--
ALTER TABLE `shop_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_details`
--
ALTER TABLE `shop_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_img`
--
ALTER TABLE `shop_img`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_owner`
--
ALTER TABLE `shop_owner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_owner_details`
--
ALTER TABLE `shop_owner_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcat`
--
ALTER TABLE `subcat`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `area`
--
ALTER TABLE `area`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `dummy_img`
--
ALTER TABLE `dummy_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `dummy_shop_detail`
--
ALTER TABLE `dummy_shop_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `img`
--
ALTER TABLE `img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `moderator`
--
ALTER TABLE `moderator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `shop_detail`
--
ALTER TABLE `shop_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `shop_details`
--
ALTER TABLE `shop_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT for table `shop_img`
--
ALTER TABLE `shop_img`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `shop_owner`
--
ALTER TABLE `shop_owner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `shop_owner_details`
--
ALTER TABLE `shop_owner_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `subcat`
--
ALTER TABLE `subcat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
