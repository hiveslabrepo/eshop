<?php
	session_start();
	include('config.php');
?>
<!DOCTYPE html>
<html>
<head>

	<title>User Profile</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/areaContent.css">
 
  <link rel="shortcut icon" href="assets/ico/rsz_final.gif">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
 
  
  <script type="text/javascript" src="jquery/jquery.js"></script>
  
  
   <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/style.css">
  
</head>
<body style="background-color:white">		
    
   
    <header>
    <div class ="navbar navbar-inverse navbar-static-top">
	
		<div class ="container">
		
			<b class="active"><a href="index.php" class ="navbar-brand">
				E-Shop
			</a></b>
			<button class ="navbar-toggle" data-toggle="collapse" data-target =".navHeaderCollapse">
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
			</button>
		
			<!--div class ="collapse navbar-collapse navHeaderCollapse">
			
				
			
			</div-->
			
			
		</div>
		
	</div>
    </header>
	<div class="container">
            <center><h1>Shop Owner Profile</h1></center>
            <a class="btn btn-info" href="signoutOwner.php" style="float:right;margin-top:-60px;">Sign Out</a>
            <div class="row">      
              <div class="col-md-12 personal-info">   
				
                <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
                   <table id="example2" class="table table-bordered table-hover" style=" margin-left:;">
					<tr>
						<th>Id</th>
						<th>Name</th>
						<th>Contact</th>
						<th>Email</th>
						<th>Address</th>
						<th>Status</th>
					</tr>
					<tr>
						<?php
							/* $sql="SELECT * FROM shop_owner_details WHERE owner_id=".$_SESSION['owner_id'];
							$res=mysqli_query($conn,$sql);
							$row=mysqli_fetch_array($res); */
							$sql="SELECT * FROM shop_owner WHERE id=".$_SESSION['owner_id'];
							$res=mysqli_query($conn,$sql);
							$row=mysqli_fetch_array($res);
							$_SESSION['status']=$row['status'];
						?>
						<td><?php echo $row['id'];?></td>
						<td><?php echo $row['name'];?></td>
						<td><?php echo $row['contact'];?></td>
						<td><?php echo $row['email'];?></td>
						<td><?php echo $row['addr'];?></td>
						<td><?php echo $row['status'];?></td>
					</tr>
				   </table>
                   
                </form>
            </div>
        
    </div>
</div>
<section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <center><h1>Shop Details</h1></center>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover" style=" margin-left:113px;">
                <thead>
                <tr>
                  
                  <th>Name</th>
                  <th>Img</th>
                  <th>Contact</th>
                  <th>Services</th>
                    <th>Category</th>
                    <th>sub-cat</th>
					<th>Area</th>
                    
                    
                </tr>
                </thead>
                <tbody>
				<?php
					if($row['status']=="pending")
					{
						$cnt=0;
						$str1="SELECT * FROM shop_owner_details WHERE owner_id=".$_SESSION['owner_id'];
						$res1=mysqli_query($conn,$str1);
						while($row1=mysqli_fetch_array($res1))
						{
				?>
                <tr>
					
					<td><?php echo $row1['name'];?></td>
					<td><img style="width:30;height:40px;" src="admin/images/<?php echo $row1['img'];?>"></td>
					<td><?php echo $row1['contact'];?></td>
					<td><?php echo $row1['services'];?></td>
					<td>
					<?php
						$str2="SELECT cat_id,name FROM subcat WHERE id=".$row1['subcat_id'];
						$res2=mysqli_query($conn,$str2);
						$row2=mysqli_fetch_array($res2);
						
						$str3="SELECT name FROM category WHERE id=".$row2['cat_id'];
						$res3=mysqli_query($conn,$str3);
						$row3=mysqli_fetch_array($res3);
						
						echo $row3['name'];
					?></td>
                    <td><?php echo $row2['name'];?></td>
					<td>
					<?php
						$str4="SELECT name FROM area WHERE id=".$row1['area_id'];
						$res4=mysqli_query($conn,$str4);
						$row4=mysqli_fetch_array($res4);
						
						echo $row4['name']; 
					?>
					</td>
                    
                    </tr>
				<?php
						}
					}	
				?>
				<?php
					if($row['status']=="approve")
					{
						$cnt=0;
						$str10="SELECT * FROM shop_detail WHERE owner_id=".$_SESSION['owner_id'];
						$res10=mysqli_query($conn,$str10);
						$row10=mysqli_fetch_array($res10);
						$_SESSION['shop_id']=$row10['id'];
						
				?>
                <tr>
					
					<td><?php echo $row10['name'];?></td>
					<td><img style="width:30;height:40px;" src="admin/images/<?php echo $row10['img'];?>"></td>
					<td><?php echo $row10['contact'];?></td>
					<td><?php echo $row10['services'];?></td>
					<td>
					<?php
						$str20="SELECT cat_id,name FROM subcat WHERE id=".$row10['subcat_id'];
						$res20=mysqli_query($conn,$str20);
						$row20=mysqli_fetch_array($res20);
						
						$str30="SELECT name FROM category WHERE id=".$row20['cat_id'];
						$res30=mysqli_query($conn,$str30);
						$row30=mysqli_fetch_array($res30);
						
						echo $row30['name'];
					?></td>
						<td><?php echo $row20['name'];?></td>
						<td>
					<?php
						$str40="SELECT name FROM area WHERE id=".$row10['area_id'];
						$res40=mysqli_query($conn,$str40);
						$row40=mysqli_fetch_array($res40);
						
						echo $row40['name']; 
					?>
					</td>
                    
                    </tr>
				<?php
						
					}	
				?>
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>
		<section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
              <center><h1>Gallery</h1></center>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover" style=" margin-left:113px;">
                <tbody>
                <tr>
				<?php
				    if($row['status']=="pending")
					{
						$str5="SELECT * FROM shop_img WHERE owner_id=".$_SESSION['owner_id'];
						$res5=mysqli_query($conn,$str5);
						$cnt=0;
						if(mysqli_num_rows($res5)>0)
						{
							while($row5=mysqli_fetch_array($res5))
							{ 
				?>
					<td style="width:200px;">
						<img src="admin/images/<?php echo $row5['img'];?>"  style="width:160px;height:100px;" >
					</td>	
				<?php 
							}
						}
						else
						{ 
				?>
						<td><a href="images.php" style="color:green;">Pending</a></td>
				<?php
						}
					}	
				?>
				<?php
				    if($row['status']=="approve")
					{
						$str5="SELECT * FROM img WHERE shop_id=".$row10['id'];
						$res5=mysqli_query($conn,$str5);
						$cnt=0;
						if(mysqli_num_rows($res5)>0)
						{
						while($row5=mysqli_fetch_array($res5))
						{ 
				?>
					<td style="width:200px;">
						<img src="admin/images/<?php echo $row5['img'];?>"  style="width:160px;height:100px;" >
					</td>	
				<?php 
							}
						}
						else
						{ 
				?>
						<td><a href="images.php" style="color:green;">Pending</a></td>
				<?php
						}
					}
				?>
												
					
                </tr>
                </tbody>
                
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>

		

<body>
</html>
<script>
$(document).ready(function()
{
	/* $('#signout').click(function()
	{
		//alert('hiii');
		$.ajax(
		{
			type:'post',
			url:'signoutOwner.php',
			success:function(data)
			{
				alert(data);
			}
		});
	}); */
});
</script>