 <?php    
			error_reporting(0);
			session_start();
            include("config.php");
 ?>
<!DOCTYPE html>
<html>
<head>
    
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
    
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">
	
  <link rel="stylesheet" href="../../bootstrap/css/mySheet.css">		
		
  <script src="jquery.js"></script>
   <script type="text/javascript">
    $(document).ready(function ()
             {
                $('#showandhide').hide();
                    $('#hideid').on('click',function()
                    {
                        var id = $('#sid').val();
                        alert(id);
                        $.ajax(
                            {
                                type:"POST",
                                url:"getSid.php",
                                data:{'id':id},
                                
                                success:function(data)
                                {
                                    alert(data);
                                    $('#').html(data);   
                                    //$('#showandhide').show();
                                }
                            });
                    });
            });
</script> 
    
<!--
    <script type="text/javascript">
//        $(document).ready(function(){
//    $("#hideid").click(function(event){
//        event.preventDefault();
//        //alert('hi');
//        $('#showandhide').hide();
//    });
//    $("#show").click(function(){
//        $("p").show();
//    });
});
</script>
-->

</head>
   
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="admin.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">
			  <?php
				$sqll="SELECT COUNT(*) FROM moderator WHERE flag=0";
				$result11=mysqli_query($conn,$sqll);
				$roow=mysqli_fetch_array($result11);
				
				
				$countApp="SELECT COUNT(*) FROM shop_owner_details WHERE flag=1";
				$countApp1=mysqli_query($conn,$countApp);
				$countApp2=mysqli_fetch_array($countApp1);
				
				$countDis="SELECT COUNT(*) FROM shop_owner_details WHERE flag=0 AND dis=1";
				$countDis1=mysqli_query($conn,$countDis);
				$countDis2=mysqli_fetch_array($countDis1);
				
				$countNewShop="SELECT COUNT(*) FROM shop_owner_details WHERE flag=0 AND dis=0";
				$countNewShop1=mysqli_query($conn,$countNewShop);
				$countNewShop2=mysqli_fetch_array($countNewShop1);
				
				$notificationCount=$roow[0]+$countApp2[0]+$countDis2[0]+$countNewShop2[0];
				echo $notificationCount;
			  ?>
			  </span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $notificationCount." ";?>notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
				<?php
					if($roow[0]>0)
					{
				?>
                  <li>
                    <a href="newMod.php">
                      <i class="fa fa-users text-aqua"></i><?php echo $roow[0]." ";?> New moderator
                    </a>
                  </li>
				  <?php
					}
					if($countApp2[0]>0)
					{
				  ?>
				  <li>
					<a href="approve-mod-admin.php">
						<i class="fa fa-users text-aqua"></i><?php echo $countApp2[0]." ";?>Approved By Moderator
					</a>
				  </li>
				<?php
					}
					if($countDis2[0]>0)
					{
				?>
				  <li>
					<a href="discard-mod-admin.php">
						<i class="fa fa-users text-aqua"></i><?php echo $countDis2[0]." "?>Discard By Moderator
					</a>
				  </li>
				<?php
					}
					if($countNewShop2[0]>0)
					{
				?>  
				  <li>
						<a href="newShopOwner.php">
							<i class="fa fa-users text-aqua"></i><?php echo $countNewShop2[0]." ";?>New Shops
						</a>
				  </li>
				<?php
					}
				?>  
                </ul>
              </li>
              <!--li class="footer"><a href="#">View all</a></li-->
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../images/<?php echo "modprofile.jpg";//../../dist/img/user2-160x160.jpg?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo "Admin"?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="../../../signup.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../images/<?php echo "modprofile.jpg";//../../dist/img/user2-160x160.jpg?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo "Admin";?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
			
           <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>New</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin.php"><i class="fa fa-circle-o"></i>Added By Moderator</a></li>
            <li><a href="approve-mod-admin.php"><i class="fa fa-circle-o"></i>Approved By Moderator</a></li>
			<li><a href="discard-mod-admin.php"><i class="fa fa-circle-o"></i>Discard By Moderator</a></li>
          </ul>
		 </li>
		 <li class="treeview">
				<a href="#">
					<i class="fa fa-plus"></i><span>Add</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-lift pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="addCity.php"><i class="fa fa-circle-o"></i>Add city</a></li>
					<li><a href="addArea.php"><i class="fa fa-circle-o"></i>Add Area</a></li>
					<li><a href="addCat.php"><i class="fa fa-circle-o"></i>Add Category</a></li>
					<li><a href="addsubCat.php"><i class="fa fa-circle-o"></i>Add Sub-category</a></li>
				</ul>
			</li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Moderators</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="existing.php"><i class="fa fa-circle-o"></i>Existing</a></li>
            <li><a href="newMod.php"><i class="fa fa-circle-o"></i>New Moderator</a></li>
          </ul>
        </li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-user"></i><span>New Shop</span>
				<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
			</a>
			<ul class="treeview-menu">
				<li><a href="newShopOwner.php"><i class="fa fa-circle-o"></i>New Shop</a></li>
			</ul>
		</li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-pencil-square-o"></i><span>Edit</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
            <li><a href="editShopByMod.php"><i class="fa fa-circle-o"></i>Shop Edited By Moderator</a></li>
            <li><a href="editShopImgByMod.php"><i class="fa fa-circle-o"></i>Shop Gallery Edited By Mod</a></li>
          </ul>
		</li>
        
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    

<!--       Table Start -->

    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">New</a></li>
        <li class="active">Added By Moderator</li>
      </ol>
    </section>
    <div class="form-group">
        
<!--                    <label class="col-lg-1 control-label"> Search :</label>-->
                    <div class="col-lg-3">
                      
					  <!--div id="myText"-->
						<label style="margin-left:45px;margin-top:1px;">Search</label>
							<input class="form-control" style="margin-top:-30px;margin-left:120px;font-size:15px;height:40px;width:200px;" placeholder="Serch" type="text" id="sid" >
					  <!--/div-->
                    </div>

     </div>
<hr>
<div id="result">
    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
				<h3 class="box-title">Add Area</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<label>Select City</label>
				<select>
					<option>--Select--</option>
					<?php
						$sql2="SELECT * FROM city";
						$res01=mysqli_query($conn,$sql2);
						while($row01=mysqli_fetch_array($res01))
						{
					?>
					<option value="<?php echo $row01['id'];?>"><?php echo $row01['name'];?></option>
					<?php
						}
					?>
				</select><br />
				<input class="myText" type="text" id="txtArea" disabled><br>
				<button class="myClass" type="button" id="addBtn"><span>Add</span></button>
				<button class="myClass" type="button" id="cancel" ><span>Cancel</span></button>
            </div>
			<!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>
			
		<section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <!-- /.box-header -->
            <div class="box-header">
				<h3 class="box-title">Existing Areas</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				<table id="example2" class="table table-bordered table-hover" style=" margin-left:;">
                <thead>
                <tr>
                  <th>Sr no.</th>
                  <th>Name</th>
				  <th>City Name</th>
                  <th>Operations</th>
                    
                </tr>
                </thead>
                <tbody>
				<?php
					$cnt=0;
					$str1="SELECT * FROM area";
					$res1=mysqli_query($conn,$str1);
					while($row1=mysqli_fetch_array($res1))
					{
				?>
                <tr>
					<td><?php echo ++$cnt;?></td>
					<td style="width:auto;"><?php echo $row1['name'];?></td>
					<td>
						<?php
							$sql="SELECT name FROM city WHERE id=".$row1['city_id'];
							$res11=mysqli_query($conn,$sql);
							$row11=mysqli_fetch_array($res11);
							echo $row11['name'];
						?>
					</td>
					<td>
					&nbsp;<button class="btn btn-warning delete-row" type="button" data-delete-id="<?php echo $row1['id'];?>">DELETE</button>
					</td>
                    </tr>
				<?php
					}
				?>	
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>
		
		</div>
      </div>
        
<!--        End-->
        <!-- Main content -->


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  
    </div>
    
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
    
    
</body>
</html>
<script>
  
$(document).ready(function()
{
	var city="";
	$('#sid').keyup(function()
	{
		//alert($('#sid').val());
		var search=$('#sid').val();
		if(search!='')
		{
				
			$.ajax(
			{
				type:'post',
				url:'searchArea.php',
				data:{'postVal':$('#sid').val()},
				success:function(data)
				{
					//alert(data);
					if(data!='')
					{
						$('#result').html(data);
					}
					else
					{
						$('#result').html("<p style='color:red'>No match found</p>");
					}
				}
			}); 
		}
		else
		{
			window.location.href="addArea.php";
		}
	});
	$("select").on('change',function()
	{
		alert($(this).val());
		city=$(this).val();
		$('#txtArea').removeAttr("disabled");
	});
	$("#addBtn").click(function()
	{
		var getArea=$("#txtArea").val();
		if(getArea!="")
		{
			
			if(confirm("Are you sure Do you want to add "+getArea))
			{
				$.ajax(
				{
					type:'post',
					url:'addAreaAdmin.php',
					data:{'postArea':getArea,'cityId':city},
					success:function(data)
					{
						alert(data);
						location.reload();
					}
				}); 
			}	
		}
	});
	
});		
$(document).on("click",".delete-row",function()
		{
			var id=$(this).data('delete-id');
			//alert(id);
			if(confirm("Are you sure?"))
			{
				$.ajax(
				{
					type:"POST",
					url:"deleteArea.php",
					data:{'postdata':id},
					success:function(data)
					{
						alert(data);
						location.reload();
					}
				});
			}
			else
			{
				location.reload();
			}
		});
</script>
