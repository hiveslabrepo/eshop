<?php include('config.php');?>
<div class="modal fade" id="view1" role="dialog">
    
        <div class="modal-dialog">
        
            <div class="modal-content">
            
                <div class="modal-header">
                 <button type="button" id="btn" class="close" data-dismiss="modal">&times;</button>
                    <h4>Shop Details</h4>
                </div>
                <div class="modal-body">
				
                <input type="hidden" id="txtId" name="id" >
                    <form>
                        <div class="form-group" for="typeahead1">
                          <label for="usr">Owner Name :</label>
                          <input type="text" name="txtName" class="form-control" id="ownerName"  disabled>
                        </div>
                        <div class="form-group">
                          <label for="usr">Owner Address :</label>
                          <input type="text" name="address" class="form-control" id="ownerAddr" disabled>
                        </div>
                        <div class="form-group">
                          <label for="usr">Owner Contact :</label>
                          <input type="text" name="contact" class="form-control" id="ownerContact" disabled>
                        </div>
						<div class="form-group">
                          <label for="usr">Shop Under :</label>
                          <input type="text" name="contact" class="form-control" id="shopCity" disabled>
                        </div>
						<div class="form-group">
                          <label for="usr">Sub-Category :</label>
                          <input type="text" name="contact" class="form-control" id="subCat" disabled>
                        </div>
						<div class="form-group">
                          <label for="usr">Approve Date :</label>
                          <input type="text" name="contact" class="form-control" id="approveDate" disabled>
                        </div>
						<div class="form-group">
							<input class="btn btn-primary" type="button" value="view Images" id="viewImg">
						</div>
						<div class="form-group">
							<div id="images"></div>
							<br>
							<div id="btnHide">
								 <input class="btn btn-primary" type="button" value="Hide Images" id="hideImg" >
							</div>							
						</div>
					</form>    
                
                </div>
               
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
<script>
$(document).ready(function()
{
	$('#btnHide').hide();
	$('#viewImg').click(function()
	{
		var hdn=$('#txtId').val();
		$.ajax(
		{
			type:'post',
			data:{'postVal':hdn},
			url:'getImages.php',
			success:function(data)
			{
				$('#btnHide').show();
				$('#images').html(data);
			}
		});
	});
	$('#hideImg').click(function()
	{
		$('#btnHide').hide();
		$('#images').html('');
	});
});
</script>