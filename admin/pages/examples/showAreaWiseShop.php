<?php
	include('config.php');
	
	if($_POST['postVal']!="--Select--")
	{
	$output="<section class='content'>
      <div class='row'>
        <div class='col-lg-12'>
          <div class='box'>
            <!-- /.box-header -->
            <div class='box-header'>
				<h3 class='box-title'>Existing Areas</h3>
            </div>
            <!-- /.box-header -->
            <div class='box-body'>
				<table id='example2' class='table table-bordered table-hover' style=' margin-left:;'>
                <thead>
                <tr>
                  <th>Sr no.</th>
                  <th>Shop Name</th>
				  <th>Shop Address</th>
				  <th>Shop Image</th>
				  <th>Contact</th>
                  <th>Operations</th>
                    
                </tr>
                </thead>
                <tbody>";
					$cnt=0;
					$str1="SELECT * FROM shop_detail WHERE area_id=".$_POST['postVal'];
					$res1=mysqli_query($conn,$str1);
					if(mysqli_num_rows($res1)>0)
					{
					while($row1=mysqli_fetch_array($res1))
					{
						
						$str2="SELECT city_id,name FROM area WHERE id=".$row1['area_id'];
						$res2=mysqli_query($conn,$str2);
						$row2=mysqli_fetch_array($res2);
						
						$str3="SELECT name FROM city WHERE id=".$row2['city_id'];
						$res3=mysqli_query($conn,$str3);
						$row3=mysqli_fetch_array($res3);
						
						$str4="SELECT cat_id,name FROM subcat WHERE id=".$row1['subcat_id'];
						$res4=mysqli_query($conn,$str4);
						$row4=mysqli_fetch_array($res4);
						
						$str5="SELECT name FROM category WHERE id=".$row4['cat_id'];
						$res5=mysqli_query($conn,$str5);
						$row5=mysqli_fetch_array($res5);
						
                $output.="<tr>
					<td>".++$cnt."</td>
					<td>".$row1['name']."</td>
					<td><div style='margin-top:15px;'>".$row1['addr']."</div></td>
					<td><img style='height:40px;width:60px;'src='../../images/".$row1['img']."'></td>
					<td>".$row1['contact']."</td>
					<td>
					<div style='margin-top:15px;'>
						<button class ='btn btn-primary open-editdialog'  type='button'  data-target='#viewShop' data-toggle='modal' data-shop-id='".$row1['id']."' data-area-name='".$row2['name']."' data-city-name='".$row3['name']."' data-subcat-name='".$row4['name']."' data-cat-name='".$row5['name']."'>View</button>
					</div>
					<div style='margin-top:-45px;margin-left:70px;'>	
						&nbsp;&nbsp;&nbsp;&nbsp;<button class='btn btn-warning delete-row' type='button' data-delete-id='".$row1['id']."'>DELETE</button>
					</div>
					</td>
                    </tr>";
				
					}
					
                $output.="</tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>";
		echo $output;
	}
}	
?>