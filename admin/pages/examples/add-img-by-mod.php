 <?php 
			session_start();
			//echo $_SESSION['img'];
            include("config.php");
 ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Moderator</title>
  <!-- Tell the browser to be responsive to screen width -->
    
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
   
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="../../modiratorprofile.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Moderator</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning">
					<?php
						$countShops="SELECT count(*) FROM shop_owner_details WHERE flag=0 AND dis=0";
						$countShops1=mysqli_query($conn,$countShops);
						$countShops2=mysqli_fetch_array($countShops1);
						echo $countShops2[0];
					?>
			  </span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $countShops2['0']." ";?> notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="approve-mod.php">
                      <i class="fa fa-users text-aqua"></i> <?php echo $countShops2['0']." ";?> Shops newly added
                    </a>
                  </li>
                </ul>
              </li>
              
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../images/<?php echo $_SESSION['img'];?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['name'];?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../images/<?php echo $_SESSION['img'];?>" class="img-circle" alt="User Image">

                <p>
                  <?php echo $_SESSION['name'];?> - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="../../modSign.php" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../images/<?php echo $_SESSION['img'];?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo $_SESSION['name'];?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
           <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>New</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
			<li><a href="create-owner-account.php"><i class="fa fa-circle-o"></i>Create Owner Account</a></li>
            <li><a href="addnewshop.php"><i class="fa fa-circle-o"></i>Add New Shop</a></li>
            <li><a href="editshop.php"><i class="fa fa-circle-o"></i>Edit Shop</a></li>
			<li><a href="approve-mod.php"><i class="fa fa-circle-o">&nbsp;&nbsp;Approve</i></a></li>
          </ul>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-table"></i> <span>Tables</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="../tables/simple.html"><i class="fa fa-circle-o"></i> Simple tables</a></li>
            <li><a href="../tables/data.html"><i class="fa fa-circle-o"></i> Data tables</a></li>
          </ul>
        </li>
        <li>
          <a href="../calendar.html">
            <i class="fa fa-calendar"></i> <span>Calendar</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-red">3</small>
              <small class="label pull-right bg-blue">17</small>
            </span>
          </a>
        </li>
        <li>
          <a href="../mailbox/mailbox.html">
            <i class="fa fa-envelope"></i> <span>Mailbox</span>
            <span class="pull-right-container">
              <small class="label pull-right bg-yellow">12</small>
              <small class="label pull-right bg-green">16</small>
              <small class="label pull-right bg-red">5</small>
            </span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">New</a></li>
        <li class="active">Add New Shop</li>
      </ol>
        <div class="container">
            <center><h1>Add Shop Images</h1></center>
            <hr>
            <div class="row">      
              <div class="col-md-12 personal-info">   
                <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
                   
				   <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Owner Id:</label>
                    <div class="col-lg-5">
                      <input type="text" name="txtId" id="shopId"><div style="color:red;" id="response"></div>
                    </div>
                  </div>
                    <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file" name="fileinput">
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file" name="fileinput1">
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file" name="fileinput2">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                      <input class="btn btn-primary" type="submit" value="Add" name="submit" >
                      <span></span>
                      <input class="btn btn-default" type="reset" value="Cancel" name="reset" ><br>
					  <div style="color:red">You have to upload atleast 3 images</div>
                    </div>
                  </div>
                </form>
            </div>
        
    </div>
</div>
<hr>
    </section>

  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <strong> <a href="http://hiveslab.com">Hives Online India Pvt Lmt</a>.</strong> 
  </footer>

  <!-- Control Sidebar -->
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
</body>
</html>
<script>
$(document).ready(function() {
    $("#shopId").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A, Command+A
            (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
            e.preventDefault();
        }
    });
	$("#shopId").focusout(function()
	{
		//alert($(this).val());
		 $.ajax(
		{
			type:'post',
			url:'checkId.php',
			data:{'postId':$(this).val()},
			success:function(data)
			{
				$("#response").html(data);
			}
		});
	});
});
</script>
<?php
 $cnt=0;
 if(isset($_POST['submit']))
 {
    //echo "<script>alert(".$_POST['txtId'].")</script>";
	$sql="SELECT id FROM shop_detail WHERE owner_id=".$_POST['txtId'];
		$res=mysqli_query($conn,$sql);
		if(mysqli_num_rows($res)>0)
		{
		$row=mysqli_fetch_array($res);
		//echo"<script>alert(".$row['id'].")</script>";
		//exit();
	$filename=$_FILES['fileinput']['name'];
	$filetype=$_FILES['fileinput']['type'];
	$filesize=$_FILES['fileinput']['size'];
	$filetemp=$_FILES['fileinput']['tmp_name'];
	
	if($filename=="")
	{
		$cnt++;
	}
	
	$filename1=$_FILES['fileinput1']['name'];
	$filetype1=$_FILES['fileinput1']['type'];
	$filesize1=$_FILES['fileinput1']['size'];
	$filetemp1=$_FILES['fileinput1']['tmp_name'];
	
	if($filename=="")
	{
		$cnt++;
	}
	
	$filename2=$_FILES['fileinput2']['name'];
	$filetype2=$_FILES['fileinput2']['type'];
	$filesize2=$_FILES['fileinput2']['size'];
	$filetemp2=$_FILES['fileinput2']['tmp_name'];
	
	if($filename=="")
	{
		$cnt++;
	}
	echo $filename;
	echo $filename1;
	echo $filename2;
	if($cnt==3)
	{
		echo"<script>alert('Please Upload at least 3 images')</script>";
	}
	 else
	{	
		 move_uploaded_file($filetemp,"admin/images/$filename");
		move_uploaded_file($filetemp1,"admin/images/$filename1");
		move_uploaded_file($filetemp2,"admin/images/$filename2");
		 
			 $str="INSERT INTO img(img,shop_id) VALUES('".$filename."',".$row['id'].")";
				if(mysqli_query($conn,$str))
				{
					//echo"inserted";
				}
					$str1="INSERT INTO img(img,shop_id) VALUES('".$filename1."',".$row['id'].")";
				if(mysqli_query($conn,$str1))
				{
					//echo"inserted";
				}
					$str2="INSERT INTO img(img,shop_id) VALUES('".$filename2."',".$row['id'].")";
				if(mysqli_query($conn,$str2))
				{
					echo"<script>alert('shop Images added Succesfully');window.location.href='create-owner-account.php'</script>";
				}
				$str4="UPDATE shop_owner SET img_flag=1 WHERE id=".$_POST['txtId'];
				mysqli_query($conn,$str4);
	 
		
	}
 }	
 }
 else
 {
	echo "<script>alert('no record Found');</script>";
 }
?>