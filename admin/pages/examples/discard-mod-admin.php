 <?php    
			error_reporting(0);
			session_start();
            include("config.php");
 ?>
<!DOCTYPE html>
<html>
<head>
    
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
    
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="../../bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <script src="jquery.js"></script>
   <script type="text/javascript">
    $(document).ready(function ()
             {
                $('#showandhide').hide();
                    $('#hideid').on('click',function()
                    {
                        var id = $('#sid').val();
                        alert(id);
                        $.ajax(
                            {
                                type:"POST",
                                url:"getSid.php",
                                data:{'id':id},
                                
                                success:function(data)
                                {
                                    alert(data);
                                    $('#').html(data);   
                                    //$('#showandhide').show();
                                }
                            });
                    });
            });
</script> 
    
<!--
    <script type="text/javascript">
//        $(document).ready(function(){
//    $("#hideid").click(function(event){
//        event.preventDefault();
//        //alert('hi');
//        $('#showandhide').hide();
//    });
//    $("#show").click(function(){
//        $("p").show();
//    });
});
</script>
-->
</head>
   
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="admin.php" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Admin</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <li class="dropdown messages-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-envelope-o"></i>
              <span class="label label-success">4</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 4 messages</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- start message -->
                    <a href="#">
                      <div class="pull-left">
                        <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
                      </div>
                      <h4>
                        Support Team
                        <small><i class="fa fa-clock-o"></i> 5 mins</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <!-- end message -->
                </ul>
              </li>
              <li class="footer"><a href="#">See All Messages</a></li>
            </ul>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="dropdown notifications-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-warning"><?php
				$sqll="SELECT COUNT(*) FROM moderator WHERE flag=0";
				$result11=mysqli_query($conn,$sqll);
				$roow=mysqli_fetch_array($result11);
				
				
				$countApp="SELECT COUNT(*) FROM shop_owner_details WHERE flag=1";
				$countApp1=mysqli_query($conn,$countApp);
				$countApp2=mysqli_fetch_array($countApp1);
				
				
				
				$notificationCount=$roow[0]+$countApp2[0];
				echo $notificationCount;
			  ?>
			  </span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have <?php echo $notificationCount." ";?>notifications</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li>
                    <a href="newMod.php">
                      <i class="fa fa-users text-aqua"></i><?php echo $roow[0]." ";?> New moderator
                    </a>
                  </li>
				  <li>
					<a href="approve-mod-admin.php">
						<i class="fa fa-users text-aqua"></i><?php echo $countApp2[0]." ";?>Approved By Moderator
					</a>
				  </li>
                </ul>
              </li>
              <!--li class="footer"><a href="#">View all</a></li-->
            </ul>
          </li>
          <!-- Tasks: style can be found in dropdown.less -->
          <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">You have 9 tasks</li>
              <li>
                <!-- inner menu: contains the actual data -->
                <ul class="menu">
                  <li><!-- Task item -->
                    <a href="#">
                      <h3>
                        Design some buttons
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Complete</span>
                        </div>
                      </div>
                    </a>
                  </li>
                  <!-- end task item -->
                </ul>
              </li>
              <li class="footer">
                <a href="#">View all tasks</a>
              </li>
            </ul>
          </li>
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="../../images/<?php echo "modprofile.jpg";//../../dist/img/user2-160x160.jpg?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo "Admin"?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="../../dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">

                <p>
                  Alexander Pierce - Web Developer
                  <small>Member since Nov. 2012</small>
                </p>
              </li>
              <!-- Menu Body -->
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
                <!-- /.row -->
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Profile</a>
                </div>
                <div class="pull-right">
                  <a href="#" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="../../images/<?php echo "modprofile.jpg";//../../dist/img/user2-160x160.jpg?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo "Admin";?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
           <li class="treeview">
          <a href="#">
            <i class="fa fa-plus"></i> <span>New</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="admin.php"><i class="fa fa-circle-o"></i>Added By Moderator</a></li>
            <li><a href="approve-mod-admin.php"><i class="fa fa-circle-o"></i>Approved By Moderator</a></li>
			<li><a href="discard-mod-admin.php"><i class="fa fa-circle-o"></i>Discard By Moderator</a></li>
          </ul>
		 </li>
		<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Moderators</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="existing.php"><i class="fa fa-circle-o"></i>Existing</a></li>
            <li><a href="newMod.php"><i class="fa fa-circle-o"></i>New Moderator</a></li>
          </ul>
        </li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-user"></i><span>New Shop</span>
				<span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
			</a>
			<ul class="treeview-menu">
				<li><a href="newShopOwner.php"><i class="fa fa-circle-o"></i>New Shop</a></li>
			</ul>
		</li>
		<li class="treeview">
			<a href="#">
				<i class="fa fa-pencil-square-o"></i><span>Edit</span>
				<span class="pull-right-container">
					<i class="fa fa-angle-left pull-right"></i>
				</span>
			</a>
			<ul class="treeview-menu">
            <li><a href="editShopByMod.php"><i class="fa fa-circle-o"></i>Shop Edited By Moderator</a></li>
            <li><a href="editShopImgByMod.php"><i class="fa fa-circle-o"></i>Shop Gallery Edited By Mod</a></li>
          </ul>
		</li>
        
		
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
    <!-- Content Header (Page header) -->
    

<!--       Table Start -->

    <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
      </ol>
    </section>
    <div class="form-group">
        
<!--                    <label class="col-lg-1 control-label"> Search :</label>-->
                    <div class="col-lg-3">
                      <input class="form-control" placeholder="Serch" type="text" id="sid">
                    </div>
                    <div class="col-lg-2">
<!--                        <input class="form-control" placeholder="Enter Shop Id" type="text" id="sid1">-->
                        <button id="hideid" class="btn btn-success">Search</button>
                    </div>
     </div>
<hr>
    <!-- Main content -->
<?php
		$sql="SELECT * FROM moderator WHERE flag=1";
		$result=mysqli_query($conn,$sql);
		while($row11=mysqli_fetch_array($result))
		{
	?>
    <section class="content">
      <div class="row">
        <div class="col-lg-12">
          <div class="box">
            <div class="box-header">
			<img src="../../images/<?php echo $row11['img'];?>" style="height:40px;width:40px;border-radius:50px;">
              <h3 class="box-title">Discard By <?php echo $row11['name'];?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example2" class="table table-bordered table-hover" style=" margin-left:;">
                <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Img</th>
                  <th>Contact</th>
                  <th>Operations</th>
                    
                </tr>
                </thead>
                <tbody>
				<?php
					$cnt=0;
					$str1="SELECT * FROM shop_owner_details WHERE flag=0 AND dis=1 AND mod_id=".$row11['id'];
					$res1=mysqli_query($conn,$str1);
					while($row1=mysqli_fetch_array($res1))
					{
				?>
                <tr>
					<td><?php echo ++$cnt;?></td>
					<td style="width:auto;"><?php echo $row1['name'];?></td>
					<td><img style="width:30;height:40px;" src="../../images/<?php echo $row1['img'];?>"></td>
					<td><?php echo $row1['contact'];?></td>
					<td>
					<?php
						$str9="SELECT name FROM area WHERE id=".$row1['area_id'];
						$res9=mysqli_query($conn,$str9);
						$row9=mysqli_fetch_array($res9);
						
						$str8="SELECT cat_id,name FROM subcat WHERE id=".$row1['subcat_id'];
						$res8=mysqli_query($conn,$str8);
						$row8=mysqli_fetch_array($res8);
						
						$str7="SELECT name FROM category WHERE id=".$row8['cat_id'];
						$res7=mysqli_query($conn,$str7);
						$row7=mysqli_fetch_array($res7);
					?>
					
					<button class ="btn btn-primary open-editdialog"  type="button"  data-target="#view2" data-toggle="modal" data-shop-name="<?php echo $row1['name'];?>" data-shop-address="<?php echo $row1['addr'];?>" data-shop-contact="<?php echo $row1['contact'];?>" data-shop-area="<?php echo $row9['name'];?>" data-shop-cat="<?php echo $row7['name'];?>" data-shop-subcat="<?php echo $row8['name'];?>" >View</button>
					<button class ="btn btn-primary open-editdialog"  type="submit"  data-approve-id="<?php echo $row1['id'];?>">Approve</button>
					&nbsp;<button class="btn btn-warning delete-row" type="button" data-delete-id="<?php echo $row1['id'];?>">Discard</button>
					</td>
                    </tr>
				<?php
					}
				?>	
                </tbody>
                <tfoot>
                </tfoot>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          </div>
        </div>
        </section>
		<?php
			}
		?>
      </div>
        
<!--        End-->
        <!-- Main content -->


  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.3.6
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
    
    </div>
    
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>

<!-- ./wrapper -->

<!-- jQuery 2.2.3 -->
<script src="../../plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="../../bootstrap/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="../../plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
    
    
</body>
</html>
<script>
 $(document).on("click",".open-editdialog",function()
{
	var shopname=$(this).data('shop-name');
	var shopaddr=$(this).data('shop-address');
	var shopcontact=$(this).data('shop-contact');
	var shoparea=$(this).data('shop-area');
	var shopcat=$(this).data('shop-cat');
	var shopsubcat=$(this).data('shop-subcat');
	
	$(".modal-body #name").val(shopname);
	$(".modal-body #addr").val(shopaddr);
	$(".modal-body #contact").val(shopcontact);
	$(".modal-body #area").val(shoparea);
	$(".modal-body #cat").val(shopcat);
	$(".modal-body #subcat").val(shopsubcat);
});
$(document).on("click",".open-editdialog1",function()
{
//alert("hiiii");
 	var approve=$(this).data('approve-id');
	//alert(approve);
	$.ajax(
			{
				type:"POST",
				url:"approveShopMod.php",
				data:{'postdata':approve},
				success:function(data)
				{
					alert(data);
					location.reload();
				}
			});
});
$(document).on("click",".delete-row",function()
		{
			var id=$(this).data('delete-id');
			//alert(id);
			 $.ajax(
			{
				type:"POST",
				url:"discardShopMod.php",
				data:{'postdata':id},
				success:function(data)
				{
					alert(data);
					location.reload();
				}
			}); 
		}); 

</script>
<?php require_once('view2-pop.php');?>