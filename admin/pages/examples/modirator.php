<?php
    //session_start();
?>
<!DOCTYPE html> 

<html>
<head>

	
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../assets/css/form-elements.css">
  <link rel="stylesheet" href="../assets/css/areaContent.css">
 
  <link rel="shortcut icon" href="../assets/ico/rsz_final.gif">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
 
  
  <script type="text/javascript" src="../jquery/jquery.js"></script>
  
  
   <link rel="stylesheet" href="../assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="../assets/css/animate.css">
        <link rel="stylesheet" href="../assets/css/magnific-popup.css">
        <link rel="stylesheet" href="../assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="../assets/css/form-elements.css">
  <link rel="stylesheet" href="../assets/css/style.css">
  
</head>
<body style="background-color:white">		
    
    <?php    
            include("config.php"); 
            
    ?>
    
    <header>
    <div class ="navbar navbar-inverse navbar-static-top">
	
		<div class ="container">
		
			<b class="active"><a href="../index.php" class ="navbar-brand">
				E-Shop
			</a></b>
			<button class ="navbar-toggle" data-toggle="collapse" data-target =".navHeaderCollapse">
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
			</button>
		
			<!--div class ="collapse navbar-collapse navHeaderCollapse">
		
				
			
			</div-->
			
		</div>
	
	</div>
    </header>
    <!--Edit Section-->
    <center>
        <div class="container">
            <center><h1>Add New Shop</h1></center>
            <hr>
            <div class="row">
              <!-- left column -->
              <!--div class="col-md-5">
                <div class="text-center">
                  <img src="//placehold.it/100" class="avatar img-circle" alt="avatar">
                  <h6>Upload a different photo...</h6>
                  <input class="form-control" type="file">
                </div>
              </div-->

              <!-- edit form column -->
                
                    
              <div class="col-md-9 personal-info">
                <!--div class="alert alert-info alert-dismissable">
                  <a class="panel-close close" data-dismiss="alert">×</a> 
                  <i class="fa fa-coffee"></i>
                  This is an <strong>.alert</strong>. Use this to show important messages to the user.
                </div>
                <h5>Personal info</h5-->
                  
                <form class="form-horizontal" role="form">
                   <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Category :</label>
                    <div class="col-lg-5">
                      <div class="ui-select">
                        <select id="cat" class="form-control">
                            <option>---Select---</option>
                          <?php
                            $str="SELECT * FROM category";
                            $res=mysqli_query($conn,$str);
                            while($row=mysqli_fetch_array($res))
                            {
                          ?>
                                <option value="<?php echo $row['id'];?>"><?php echo $row['name']; ?></option>
                          <?php
                            }
                            
                           ?>    
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Sub-Category :</label>
                    <div class="col-lg-5">
                      <div class="ui-select">
                        <select id="subcat" class="form-control">
                         
                          
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="form-group">
                  <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Shop name:</label>
                    <div class="col-lg-5">
                      <input class="form-control" value="Enter Shop Name" type="text">
                    </div>
                  </div>
                  <!--div class="form-group">
                    <label class="col-lg-5 control-label">Last name:</label>
                    <div class="col-lg-5">
                      <input class="form-control" value="Bishop" type="text">
                    </div>
                  </div-->
                  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Contact No:</label>
                    <div class="col-lg-5">
                      <input class="form-control" value="Enter Contact No." type="text" maxlength="10">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Email:</label>
                    <div class="col-lg-5">
                      <input class="form-control" value="Enter Your E-Mail ID" type="text" required data-validation-required-message="Please enter your company Email.">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label"><!-- style="text-align:left"-->Services:</label>
                    <div class="col-md-5">
                      <input class="form-control" value="Enter Services" type="textarea">
                    </div>
                  </div>
                  <!--div class="form-group">
                    <label class="col-md-5 control-label" ><!-- style="text-align:left">Confirm password:</label>
                    <div class="col-md-5">
                      <input class="form-control" value="11111122555" type="password">
                    </div>
                  </div-->
                    <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                      <input class="btn btn-primary" value="Submit" type="button">
                      <span></span>
                      <input class="btn btn-default" value="Cancel" type="reset">
                    </div>
                  </div>
                </form>
            </div>
        
    </div>
</div>
        </center>
<hr>
    
    <!--End-->
</body>
</html>
<script>
    $(document).ready(function ()
             {
                    $('#cat').on('change',function()
                    {
                        alert($(this).val());
                        $.ajax(
                            {
                                type:'post',
                                url:'getCat.php',
                                data:{'postData':$(this).val()},
                                success:function(data)
                                {
                                    alert(data);
                                    $('#subcat').html(data);
                                    
                                }
                            });
                    });
            });
</script>    