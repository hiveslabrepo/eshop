<?php
	session_start();
	include('config.php');
?>
<!DOCTYPE html>
<html>
<head>

	
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/areaContent.css">
 
  <link rel="shortcut icon" href="assets/ico/rsz_final.gif">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
 
  
  <script type="text/javascript" src="jquery/jquery.js"></script>
  
  
   <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/style.css">
  
</head>
<body style="background-color:white">		
    
   
    <header>
    <div class ="navbar navbar-inverse navbar-static-top">
	
		<div class ="container">
		
			<b class="active"><a href="index.php" class ="navbar-brand">
				E-Shop
			</a></b>
			<button class ="navbar-toggle" data-toggle="collapse" data-target =".navHeaderCollapse">
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
			</button>
		
			<!--div class ="collapse navbar-collapse navHeaderCollapse">
			
				
			
			</div-->
			
			
		</div>
	
	</div>
    </header>
	<div class="container">
            <center><h1>Add Your Shop Now</h1></center>
            <hr>
            <div class="row">      
              <div class="col-md-12 personal-info">   
                <form class="form-horizontal" role="form" action="" method="post" enctype="multipart/form-data">
                   <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Category :</label>
                    <div class="col-lg-5">
                      <div class="ui-select">
                        <select id="cat" class="form-control" name="cat">
                            <option>---Select---</option>
                          <?php
                            $str="SELECT * FROM category";
                            $res=mysqli_query($conn,$str);
                            while($row=mysqli_fetch_array($res))
                            {
                          ?>
                                <option value="<?php echo $row['id'];?>"><?php echo $row['name']; ?></option>
                          <?php
                            }
                            
                           ?>    
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Sub-Category :</label>
                    <div class="col-lg-5">
                      <div class="ui-select">
                        <select id="subcat" class="form-control" name="subcat" disabled>
							
                          
                        </select>
                      </div>
                    </div>
                  </div>
				  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Area :</label>
                    <div class="col-lg-5">
                      <div class="ui-select">
                        <select id="area" class="form-control" name="area">
						 <option>---Select---</option>
							<?php
                            $str1="SELECT * FROM area";
                            $res1=mysqli_query($conn,$str1);
                            while($row1=mysqli_fetch_array($res1))
                            {
                          ?>
                                <option value="<?php echo $row1['id'];?>"><?php echo $row1['name']; ?></option>
                          <?php
                            }
                            
                           ?>
                          
                        </select>
                      </div>
                    </div>
                  </div>
                <div class="form-group">
                  <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Shop name:</label>
                    <div class="col-lg-5">
                      <input class="form-control" style="color:black;"  placeholder="Enter Shop Name" name="txtName" type="text">
                    </div>
                  </div>
                  
				  <div class="form-group">
                  <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Shop Addr:</label>
                    <div class="col-lg-5">
                      <input class="form-control" style="color:black;"  placeholder="Enter Shop Addr" name="txtAddr" type="text">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Contact No:</label>
                    <div class="col-lg-5">
                      <input class="form-control" style="color:black;"  placeholder="Enter Contact No." name="txtContact" type="text" maxlength="10">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Email:</label>
                    <div class="col-lg-5">
                      <input class="form-control" style="color:black;"  placeholder="Enter Your E-Mail ID" type="text" name="txtMail" required >
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-5 control-label"><!-- style="text-align:left"-->Services:</label>
                    <div class="col-md-5">
                      <input class="form-control" style="color:black;" placeholder="Enter Services" type="textarea" name="txtServices">
                    </div>
                  </div>
                  <!--div class="form-group">
                    <label class="col-md-5 control-label" ><!-- style="text-align:left">Confirm password:</label>
                    <div class="col-md-5">
                      <input class="form-control" value="11111122555" type="password">
                    </div>
                  </div-->
                    <div class="form-group">
                    <label class="col-lg-5 control-label"><!-- style="text-align:left"-->Uploade Image:</label>
                    <div class="col-lg-5">
                      <input type="file" name="fileinput">
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                      <input class="btn btn-primary" type="submit" value="Submit" name="submit" >
                      <span></span>
                      <input class="btn btn-default" type="reset" value="Cancel" name="reset" >
                    </div>
                  </div>
                </form>
            </div>
        
    </div>
</div>
<hr>
<body>
</html>
<script>
$(document).ready(function ()
             {
                    $('#cat').on('change',function()
                    {
                        //alert($(this).val());
                        $.ajax(
                            {
                                type:'post',
                                url:'admin/pages/examples/getCat.php',
                                data:{'postData':$(this).val()},
                                success:function(data)
                                {
                                    //alert(data);
                                    $('#subcat').html(data);
                                    $('#subcat').removeAttr('disabled');
                                }
                            });
                    });
            });
</script>
<?php
if(isset($_POST['submit']))
{
	/* echo $_POST['cat'];
	echo $_POST['subcat'];
	echo "<br />".$_POST['area']; */
	/* echo $_POST['txtName'];
	echo $_POST['txtAddr'];
	echo $_POST['txtContact'];
	echo $_POST['txtMail'];
	echo $_POST['txtServices'];
	 */ 
	$filename=$_FILES['fileinput']['name'];
	$filetype=$_FILES['fileinput']['type'];
	$filesize=$_FILES['fileinput']['size'];
	$filetemp=$_FILES['fileinput']['tmp_name'];
	move_uploaded_file($filetemp,"admin/images/$filename"); 
	$str="SELECT id FROM subcat WHERE name like'".$_POST['subcat']."'";
	$res=mysqli_query($conn,$str);
	$row=mysqli_fetch_array($res);
	//echo "<br />".$row['id'];
	$str1="INSERT INTO shop_owner_details(name,addr,img,contact,email,services,subcat_id,area_id,owner_id) VALUES('".$_POST['txtName']."','".$_POST['txtAddr']."','".$filename."',".$_POST['txtContact'].",'".$_POST['txtMail']."','".$_POST['txtServices']."',".$row['id'].",".$_POST['area'].",".$_SESSION['owner_id'].")";
	if(mysqli_query($conn,$str1))
	{
		$str2="SELECT * FROM shop_owner_details";
		$res2=mysqli_query($conn,$str2);
		while($row2=mysqli_fetch_array($res2))
		{
		
		}
		$_SESSION['id']=$row2['id'];
		$str4="UPDATE shop_owner SET flag=1 WHERE id=".$_SESSION['owner_id'];
		if(mysqli_query($conn,$str4))
		{
		echo"<script>alert('your shop added successfully please wait for confirmation');window.location.href='images.php'</script>";
		}
	}
}
?>