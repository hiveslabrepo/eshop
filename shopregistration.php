<?php 
	error_reporting(0);
?>
<!DOCTYPE html> 

<html>
<head>

	<title>Moderator Login</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/areaContent.css">
 
  <link rel="shortcut icon" href="assets/ico/rsz_final.gif">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="bootstrap/js/bootstrap.min.js"></script>
 
  
  <script type="text/javascript" src="jquery/jquery.js"></script>
  
  
   <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/animate.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/flexslider/flexslider.css">
        <link rel="stylesheet" href="assets/css/form-elements.css">
  <link rel="stylesheet" href="assets/css/style.css">
  
</head>
<body style="background-color:white">		
    
   
    <header>
    <div class ="navbar navbar-inverse navbar-static-top">
	
		<div class ="container">
		
			<b class="active"><a href="index.php" class ="navbar-brand">
				E-Shop
			</a></b>
			<button class ="navbar-toggle" data-toggle="collapse" data-target =".navHeaderCollapse">
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
				<span class ="icon-bar"></span>
			</button>
		
			<!--div class ="collapse navbar-collapse navHeaderCollapse">
		
				
			
			</div-->
			
		</div>
	
	</div>
    </header>
		
		<div class="container" >
		<center><b><h1>Sign Up</h1></b>
                <p> Shop Owner </p></center>
            <hr style="width:60%">
            
			<div class="row">
				<div class="col-md-9 personal-info">
				 <center>
					<form class="form-horizontal" role="form" method="post" action="" enctype="multipart/form-data" style="margin-top:20px;">
						<div class="form-group">
							<label class="col-lg-5 control-label"><!-- style="text-align:left"-->Name :</label>
							<div class="col-lg-5">
								<div class="ui-select">
									<input class="form-control" type="textarea" placeholder="Enter Name" name="txtName">
								</div>	
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label"><!-- style="text-align:left"-->Contact :</label>
							<div class="col-lg-5">
								<div class="ui-select">
									<input class="form-control" type="textarea" placeholder="Enter Contact" name="txtContact">
								</div>	
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label"><!-- style="text-align:left"-->Address :</label>
							<div class="ui-select">
								<div class="col-lg-5">
									<input class="form-control" type="textarea" placeholder="Enter Address" name="txtAddr">
								</div>	
							</div>
						</div>
						<div class="form-group">
						
							<label class="col-lg-5 control-label"><input  type="checkbox" style="margin-right:10px;"id="check">Email :</label>
							<div class="col-lg-5">
								<div class="ui-select">
								
									<input style="color:black;" class="form-control" type="text" id="email" placeholder="Enter Email ID" name="txtEmail" disabled="disabled">
								<div style="color:red;" id="validEmail"></div>
								</div>	
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label"><!-- style="text-align:left"-->Password :</label>
							<div class="col-lg-5">
								<div class="ui-select">
									<input class="form-control" type="password" placeholder="Enter Password" name="txtPass" id="password">
								</div>
								<div id="result" style="color:red;"></div>								
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-5 control-label"><!-- style="text-align:left"-->Confirm Password :</label>
							<div class="col-lg-5">
								<div class="ui-select">
									<input class="form-control" type="password" placeholder="Confirm Password" name="txtCpass" id="cpass">
								</div>	
							</div>
						</div>
						<!--div class="form-group">
							<label class="col-lg-5 control-label">Image :</label>
							<div class="col-lg-5">
								<div class="col-lg-5">
									<input  type="file" name="fileinput">
								</div>	
							</div>
						</div-->
						<div class="form-group">
                    <label class="col-md-4 control-label"></label>
                    <div class="col-md-5">
                      <input class="btn btn-primary" value="Submit" name="submit" type="submit">
                      <span></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <input class="btn btn-default" value="Cancel" type="reset">
                    </div>
                  </div>
					</form>
				 </center>	
				</div>
			</div>
		</div>
		
	</body>

</html>
<script>
$(document).ready(function()
{
	/* var pass;
	$("#password").focusout(function () {
    //alert("hello");
		pass=$("#password").val();
		alert(pass);
	}); */
		$("#cpass").focusout(function () 
		{
    //alert("hello");
			if(!($("#cpass").val()==$("#password").val()))
			{
				$('#result').text(" Password not match");
			}
			
		});
		$(":checkbox").click(function()
		{
			 $('input:text').attr('disabled',! this.checked)
		}); 
		 function ValidateEmail(email) {
        var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return expr.test(email);
    };
	$('#email').focusout(function()
	{
		if (!ValidateEmail($("#email").val())) {
            //alert("Invalid email address.");
			$('#validEmail').html('Invalid email address');
        }
        else {
            //alert("Valid email address.");
        }
	});
	$('#email').focusin(function()
	{
		$('#validEmail').html('');
	});
});
</script>
<?php
	 include('config.php');
	if(isset($_POST['submit']))
	{
				 /* echo"hii";
				 echo $_POST['txtName'];
				echo $_POST['txtContact'];
				echo $_POST['txtAddr'];
				 
				 echo $_POST['txtCpass'];
 */				if($_POST['txtEmail']=="")
				{
					 $_POST['txtEmail']="NA";
				}				
				 /* $filename=$_FILES['fileinput']['name'];
			  $filetype=$_FILES['fileinput']['type'];
			  $filesize=$_FILES['fileinput']['size'];
			  $filetemp=$_FILES['fileinput']['tmp_name'];
			  move_uploaded_file($filetemp,"images/$filename");
			   if($filename=="")
			   {
				echo"please select the image";
				$filename="modprofile.jpg";
			   } */
				$str="INSERT INTO shop_owner(name,contact,addr,email,pass,status) VALUES('".$_POST['txtName']."','".$_POST['txtContact']."','".$_POST['txtAddr']."','".$_POST['txtEmail']."','".$_POST['txtCpass']."','pending')";
			   if(mysqli_query($conn,$str))
			   {
				$last_id = mysqli_insert_id($conn);
				
				echo"<script>alert('signup successful your id is".$last_id."');window.location.href='shoplogin.php';</script>";
			   }
			   else
			   {
				echo"error";
			   }
	} 
?>